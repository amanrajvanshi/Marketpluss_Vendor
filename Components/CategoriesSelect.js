import React, { Component } from 'react';
import {
    View,ImageBackground,
    StyleSheet,Pressable,
    Image,Text, TouchableOpacity
} from 'react-native';
import { DataTable } from 'react-native-paper';
import {Icon,Input} from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';


const category =[
    {
        id:"1",
        name:"Gym",
        img:require("../img/icons/gym.png")
    },
    {
        id:"2",
        name:"Salon",
        img:require("../img/icons/salon.png")
    },
    {
        id:"3",
        name:"Spa",
        img:require("../img/icons/spa.png")
    },
    {
        id:"4",
        name:"Activities",
        img:require("../img/icons/activities.png")
    },
    {
        id:"5",
        name:"Restaurant",
        img:require("../img/icons/restaurant.png")
    },
    {
        id:"6",
        name:"Health Care",
        img:require("../img/icons/health.png")
    },
    {
        id:"7",
        name:"Fashion",
        img:require("../img/icons/fashion.png")
    },
    {
        id:"8",
        name:"Mobile Accessories",
        img:require("../img/icons/mobile.png")
    },
    {
        id:"9",
        name:"Gadgets",
        img:require("../img/icons/gadgets.png")
    },
    {
        id:"10",
        name:"Electronics",
        img:require("../img/icons/electronics.png")
    },
    {
        id:"11",
        name:"Live Plants",
        img:require("../img/icons/plants.png")
    },
    {
        id:"12",
        name:"Laptop Accessories",
        img:require("../img/icons/laptop.png")
    },
    {
        id:"13",
        name:"Home Decor",
        img:require("../img/icons/decor.png")
    },
    {
        id:"14",
        name:"Party Essentials",
        img:require("../img/icons/party.png")
    },
    {
        id:"15",
        name:"Grocery",
        img:require("../img/icons/grocery.png")
    },
]



class CategoriesSelect extends Component{
    constructor(props){
        super(props);
        this.state={
            selected:false,
            object:{}
        }
    }

    componentDidMount(){
        category.map((value)=>{
            const object=this.state.object;
            object[value.id]=false;
            this.setState({object})
        })
    }

    update_cat(str)
    {
        const object=this.state.object;
        if(object[str])
        {
            object[str]=false;
        }
        else
        {
            object[str]=true; 
        }
        
        this.setState({object})
    }

    render(){
        
        return(
            <View>
                <View style={{flexDirection:"row",justifyContent:"center",marginTop:10}}>
                {/* first row */}
                
                {
                    
                    category.slice(0,3).map((value)=>{
                        return(
                            (!this.state.object[value.id])?
                            
                            <TouchableOpacity style={style.button} onPress={()=>{this.update_cat(value.id)}}>
                                    <Image source={value.img}
                                    style={style.image} />
                                    <Text style={[style.text,{color:"black"}]}>{value.name}</Text>
                            </TouchableOpacity>
                            
        
                        :
                        <TouchableOpacity style={[style.button,{backgroundColor:"#3e3737"}]}  onPress={()=>{this.update_cat(value.id)}}>
                                <Image source={value.img}
                                style={style.image} />
                                <Text style={[style.text,{color:"white"}]}>{value.name}</Text>
                        </TouchableOpacity>
                            
                        )
                    })
                    
                }               
</View>
<View style={{flexDirection:"row",justifyContent:"center",marginTop:10}}>
                {/* Row row */}
                
                {
                    
                    category.slice(3,6).map((value)=>{
                        return(
                            (!this.state.object[value.id])?
                            
                            <TouchableOpacity style={style.button} onPress={()=>{this.update_cat(value.id)}}>
                                    <Image source={value.img}
                                    style={style.image} />
                                    <Text style={[style.text,{color:"black"}]}>{value.name}</Text>
                            </TouchableOpacity>
                            
        
                        :
                        <TouchableOpacity style={[style.button,{backgroundColor:"#3e3737"}]}  onPress={()=>{this.update_cat(value.id)}}>
                                <Image source={value.img}
                                style={style.image} />
                                <Text style={[style.text,{color:"white"}]}>{value.name}</Text>
                        </TouchableOpacity>
                            
                        )
                    })
                    
                }               
</View>
<View style={{flexDirection:"row",justifyContent:"center",marginTop:10}}>
                {/* Row row */}
                
                {
                    
                    category.slice(6,9).map((value)=>{
                        return(
                            (!this.state.object[value.id])?
                            
                            <TouchableOpacity style={style.button} onPress={()=>{this.update_cat(value.id)}}>
                                    <Image source={value.img}
                                    style={style.image} />
                                    <Text style={[style.text,{color:"black"}]}>{value.name}</Text>
                            </TouchableOpacity>
                            
        
                        :
                        <TouchableOpacity style={[style.button,{backgroundColor:"#3e3737"}]}  onPress={()=>{this.update_cat(value.id)}}>
                                <Image source={value.img}
                                style={style.image} />
                                <Text style={[style.text,{color:"white"}]}>{value.name}</Text>
                        </TouchableOpacity>
                            
                        )
                    })
                    
                }               
</View>
<View style={{flexDirection:"row",justifyContent:"center",marginTop:10}}>
                {/* Row row */}
                
                {
                    
                    category.slice(9,12).map((value)=>{
                        return(
                            (!this.state.object[value.id])?
                            
                            <TouchableOpacity style={style.button} onPress={()=>{this.update_cat(value.id)}}>
                                    <Image source={value.img}
                                    style={style.image} />
                                    <Text style={[style.text,{color:"black"}]}>{value.name}</Text>
                            </TouchableOpacity>
                            
        
                        :
                        <TouchableOpacity style={[style.button,{backgroundColor:"#3e3737"}]}  onPress={()=>{this.update_cat(value.id)}}>
                                <Image source={value.img}
                                style={style.image} />
                                <Text style={[style.text,{color:"white"}]}>{value.name}</Text>
                        </TouchableOpacity>
                            
                        )
                    })
                    
                }               
</View>
<View style={{flexDirection:"row",justifyContent:"center",marginTop:10}}>
                {/* Row row */}
                
                {
                    
                    category.slice(12,15).map((value)=>{
                        return(
                            (!this.state.object[value.id])?
                            
                            <TouchableOpacity style={style.button} onPress={()=>{this.update_cat(value.id)}}>
                                    <Image source={value.img}
                                    style={style.image} />
                                    <Text style={[style.text,{color:"black"}]}>{value.name}</Text>
                            </TouchableOpacity>
                            
        
                        :
                        <TouchableOpacity style={[style.button,{backgroundColor:"#3e3737"}]}  onPress={()=>{this.update_cat(value.id)}}>
                                <Image source={value.img}
                                style={style.image} />
                                <Text style={[style.text,{color:"white"}]}>{value.name}</Text>
                        </TouchableOpacity>
                            
                        )
                    })
                    
                }               
</View>
               

                {/* second row */}
                {/* <View style={{flexDirection:"row",justifyContent:"center"}}>
                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/activities.png")}
                            style={style.image} />
                            <Text style={style.text}>Activities</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/restaurant.png")}
                            style={style.image} />
                            <Text style={style.text}>Restaurant</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/health.png")}
                            style={style.image} />
                            <Text style={style.text}>Health Care</Text>
                    </TouchableOpacity>
                </View> */}

                {/* third row */}
                {/* <View style={{flexDirection:"row",justifyContent:"center"}}>
                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/fashion.png")}
                            style={style.image} />
                            <Text style={style.text}>Fashion</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/mobile.png")}
                            style={style.image} />
                            <Text style={style.text}>Mobile Accessories</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/gadgets.png")}
                            style={style.image} />
                            <Text style={style.text}>Gadgets</Text>
                    </TouchableOpacity>
                </View> */}

                {/* fourth row
                <View style={{flexDirection:"row",justifyContent:"center"}}> */}
                    {/* <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/electronics.png")}
                            style={style.image} />
                            <Text style={style.text}>Electronics</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/plants.png")}
                            style={style.image} />
                            <Text style={style.text}>Live Plants</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/laptop.png")}
                            style={style.image} />
                            <Text style={style.text}>Laptop Accessories</Text>
                    </TouchableOpacity>
                </View> */}

                {/* fifth row */}
                {/* <View style={{flexDirection:"row",justifyContent:"center"}}>
                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/decor.png")}
                            style={style.image} />
                            <Text style={style.text}>Home Decor</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.button}>
                            <Image source={require("../img/icons/party.png")}
                            style={style.image} />
                            <Text style={style.text}>Party Essentials</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={style.button}
                        onPress={()=>this.props.navigation.navigate("ShopsList")}>
                            <Image source={require("../img/icons/grocery.png")}
                            style={style.image} />
                            <Text style={style.text}>Grocery</Text>
                    </TouchableOpacity> */}
                {/* </View> */}
            </View>
            
        )
    
    }
}

export default CategoriesSelect;

const style=StyleSheet.create({
    image:{
        height:40,
        width:40,
        alignContent:"center",
        alignSelf:"center",
        alignItems:"center",
    },
    text:{
        fontFamily:"Roboto-Regular",
        fontSize:13,
        textAlign:"center",
        marginTop:5
    },
    button:{
        padding:10,
        height:95,
        width:"25%",
        // borderWidth:0.2,
        backgroundColor:"#fff",
        margin:10,
        borderRadius:20,
        shadowColor: 'grey',
        shadowOpacity: 1.5,
        elevation: 10,
        shadowRadius: 10,
        shadowOffset: { width:1, height: 1 },
    }
})