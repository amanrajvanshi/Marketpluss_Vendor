import React, { Component } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Packages from '../Screens/Packages';
import Services from '../Screens/Services';


const Tabs = createMaterialTopTabNavigator();

class TopTab extends Component{
  render(){
    return(
      <Tabs.Navigator 
      initialRouteName="Service"  
      tabBarPosition="top"  
      lazy="true"
      tabBarOptions={
        {
        tabStyle: { marginTop:24 },
        labelPosition: "below-icon",
        activeTintColor: "#326bf3",
        inactiveTintColor:"#c0c0c0",
        style: {
        backgroundColor: "white",
        // height:55,
        
      },
      
      labelStyle: {
        fontSize: 18,
        fontFamily:"Raleway-Bold"
        // paddingBottom:5,
      },
    }}>
      
      <Tabs.Screen name="Service" component={Services} options={{title:"Services"}}/>
      <Tabs.Screen name="Pack" component={Packages} options={{title:"Packages"}} />
      
    </Tabs.Navigator>
    
    )
  }
}
export default TopTab