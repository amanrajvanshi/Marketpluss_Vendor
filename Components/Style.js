import {StyleSheet } from 'react-native';

module.exports =  StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:'#ffffff'
    },

    h1: {
        fontSize:32,
        fontFamily:"Ralway-SemiBold",
      },

      h2: {
        fontSize:24,
        fontFamily:"Raleway-SemiBold",
        color:'#222222'
      },

      h3: {
        fontSize:18.72,
        fontFamily:"Raleway-SemiBold",
        color:'#222222'
      },

      h4: {
        fontSize:16,
        fontFamily:"Raleway-SemiBold",
        color:'#222222'
      },

      h5: {
        fontSize:13.28,
        fontFamily:"Raleway-SemiBold",
        color:'#222222'
      },

      h6: {
        fontSize:12,
        fontFamily:"Raleway-SemiBold",
        color:'#222222'
      },
      smallHeading:
      {
        fontSize:15,
        fontFamily:"Raleway-SemiBold",
        color:'#5d5d5d',
      },
      small:
      {
        fontSize:14,
        fontFamily:"Raleway-Regular",
        color:'#5d5d5d',
        marginLeft:15
      },
      p:
      {
        fontSize:15,
        fontFamily:"Raleway-Regular",
        marginTop:5,
        color:'#5d5d5d',
      },
      signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        flexDirection:"row",
        justifyContent:"space-evenly"
    },
    textSignIn:{
      fontFamily:"Roboto-Bold",
      fontSize:18,
    },
    heading:{
        color:"#1F449B",
       // color:"#000",
        fontSize:24,
        fontFamily:"Raleway-Bold",
        marginTop:10,
        marginLeft:15
    },
    buttonStyles:{
      width:"30%",
      justifyContent:"flex-end",
      alignSelf:"flex-end",
      marginTop:25,
      marginRight:5
    },
    textInput: {
      marginTop: 20,
      //borderWidth: 0.2,
      backgroundColor: '#fff',
      borderRadius:5,
      width: "80%",
      height: 50,
      alignContent: 'center',
      alignSelf: 'center',
      shadowColor: 'grey',
      shadowOpacity: 1.5,
      elevation: 2,
      shadowRadius: 10,
      shadowOffset: { width:1, height: 1 },
      fontSize:20,
      fontFamily:"Roboto-Regular",
    },
    card:{
      backgroundColor:"#fff",
      height:215
    },
    arrow:{
    
      left:12,
      top:30
  },
  headingSmall:{
    fontSize:12,
    fontFamily:"Raleway-Medium",
    color:"#000"
  }
  });
