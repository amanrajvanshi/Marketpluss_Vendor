import React, { Component } from 'react';
import {
    View,
    StyleSheet,ActivityIndicator,TouchableOpacity,Alert,
    Image,Text,Dimensions, ScrollView, ToastAndroid
} from 'react-native';

import {Header,Icon} from "react-native-elements";
import Share from 'react-native-share';
import RBSheet from "react-native-raw-bottom-sheet";
import Toast from 'react-native-simple-toast';

//Global Style Import
const styles = require('../Components/Style.js');

//this is the component for Post 
class Post extends Component{

    constructor(props) {
      super(props);
      // Don't call this.setState() here!
      this.state = {data:[],object:{},save:{},  isLoading: true};
    }
  
    componentDidMount()
    {
      fetch('https://healthyrabbit.in/hnn/public/api/news')
      .then((response) => response.json())
      .then((json) => {
        
        this.setState({ data: json })
        json.map((id)=>{
          const object = this.state.object;
          const save =this.state.save;
          object[id] = false;
          save[id] = false;
          this.setState({ object });
          this.setState({ save });
          });
          return json;
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
    }
  
    myShare = async (title,content,url)=>{
      const shareOptions={
          title:title,
          message:content,
          url:url,
      }
      try{
          const ShareResponse = await Share.open(shareOptions);
  
      }catch(error){
          console.log("Error=>",error)
      }
  }
  

  myShare = async (title,content,url)=>{
    const shareOptions={
        title:title,
        message:content,
        url:url,
    }
    try{
        const ShareResponse = await Share.open(shareOptions);

    }catch(error){
        console.log("Error=>",error)
    }
}
  
    update_dept = (item_id) =>
      {
          const object = this.state.object;
          if(this.state.object[item_id])
          {
              object[item_id] = false;
          }
          else
          {
              object[item_id] = true;
          }
          
          
          this.setState({ object });
      }
  
      bookmark = (save_id) =>
      {
        // alert(save_id)
          const save = this.state.save;
          if(this.state.save[save_id])
          {
              save[save_id] = false;
          }
          else
          {
              save[save_id] = true;
          }
          this.setState({ save });
      }
  
      alertFunc=()=>{
        Alert.alert(
          "Are you sure?",
          "Delete this Post",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => Toast.show("Deleted Successfully") }
          ]
        )
      }

      update_dept = (item_id) =>
      {
         const object = this.state.object;
         if(this.state.object[item_id])
         {
             object[item_id] = false;
         }
         else
         {
             object[item_id] = true;
         }
         
         
         this.setState({ object });
     }
  
    render(){
  
      const { data, isLoading, object,save } = this.state;
      if(isLoading)
      {
        return(
          <View >
            <ActivityIndicator size="large" color="#326bf3" style={{marginTop:200}}/>
          </View>
        )
      }
     else{
       let news=data.map((news,id)=>{
        return(
          <View>
            {/* this is the card component for post */}
            <View style={style.card}>
              {/* Card Header */}
              <View style={style.cardHeader}>
                <TouchableOpacity style={{flexDirection:"row"}} onPress={()=>this.props.navigation.navigate("Profile")}>

                    {/* Profile Image */}
                <Image source={require("../img/ab.jpg")} style={style.profileImage}/>
               
               {/* name and type of shop */}
               <View style={{flexDirection:"column",paddingLeft:15}}>
                  <Text style={style.name}>Barbeque Nation</Text>
                  <Text style={style.type}>Restaurant</Text>
                </View>
                </TouchableOpacity>

                {/* edit icon */}
                  <Text style={[styles.h4,{alignContent:"flex-end",top:5}]}>
                    <Icon type="ionicon" name="ellipsis-vertical" onPress={()=>this.RBSheet.open()}/>
                    {/* <Image source={require("../img/icons/pencil.png")} style={style.editIcon}/> */}
                  </Text>
              </View>
          {/* Bottom Sheet for Post options */}

          <RBSheet
                            ref={ref=>{this.RBSheet = ref;}}
                            closeOnDragDown={true}
                            closeOnPressMask={true}
                            height={170}
                            customStyles={{
                                container: {
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                  },
                            draggableIcon: {
                                backgroundColor: ""
                            }
                            }}
                        >
                            {/* bottom sheet elements */}
                        <View >
                            {/* new container search view */}
                                <View>
                                    {/* to share */}
                                    <View style={{flexDirection:"row",padding:10}}>
                                    <TouchableOpacity style={{flexDirection:"row"}} 
                                    onPress={()=>this.myShare(news.heading,"https://greenrabbit.in/hnn/public/news-content/"+news.id)}>
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,justifyContent:"center",borderRadius:50}}>
                                        <Icon type="ionicon" name="share-social"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}>
                                        Share</Text>
                                        </TouchableOpacity>
                                    </View>

                                    {/* to report */}
                                    <View style={{flexDirection:"row",padding:10}}>
                                     

                                        <TouchableOpacity style={{flexDirection:"row"}} onPress={()=>this.alertFunc()
                                          }>
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,justifyContent:"center",borderRadius:50}} >
                                        <Icon type="ionicon" name="trash-bin"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}
                                        >Delete</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
        
       
           
                        </View>
                        </RBSheet>
              {/* Post Image */}
              <View>
                <View>
                <Image source={require("../img/food.jpg")}
                style={style.postImage}
                PlaceholderContent={<ActivityIndicator size="small" color="#0000ff" />}/>
                <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                  
                  {/* view for icons */}
                  <View style={{flexDirection:"row"}}>

                <View 
                style={{margin:3, marginLeft:10,flexDirection:"row"}}>
                <Icon type="ionicon" name="eye-outline" size={27}/>
                <Text style={[styles.h5,{fontFamily:"Roboto-Regular",marginTop:6,marginLeft:5}]}>21,127</Text>
                </View>
  
                <View
                style={{margin:3,marginTop:4, marginLeft:10,flexDirection:"row"}}>
                  <Text >
                <Icon type="ionicon" name={object[id] ? "heart" : "heart-outline"}
                color={object[id] ? "red" : "black"}
               onPress={() => this.update_dept(id)} size={27}/>
              </Text>
                <Text style={[styles.h5,{fontFamily:"Roboto-Regular",marginTop:4,marginLeft:5}]}>42</Text>
                </View>

                <View
                style={{margin:3,marginTop:6, marginLeft:10,flexDirection:"row"}}>
                <Icon type="ionicon" name="chatbubble-outline" size={23} onPress={()=>this.props.navigation.navigate("Comments")}/>
                <Text style={[styles.h5,{fontFamily:"Roboto-Regular",marginTop:4,marginLeft:5}]}>9</Text>
                </View>

                <View
                style={{margin:3,marginTop:6, marginLeft:10,flexDirection:"row"}}>
                <Icon type="ionicon" name="share-social" size={23} onPress={()=>this.myShare(news.heading,"https://greenrabbit.in/hnn/public/news-content/"+news.id)}/>
                <Text style={[styles.h5,{fontFamily:"Roboto-Regular",marginTop:4,marginLeft:5}]}>4</Text>
                </View>
  
              
                </View>

                {/* <View>
                <Text  style={{margin:5,justifyContent:"flex-end"}}>
                <Icon type="ionicon"  name={save[id] ? "bookmark" : "bookmark-outline"}
                color={save[id] ? "black" : "black"}
                onPress={() => this.bookmark(id)}
                 size={25} />
                 </Text>
                 </View> */}
  
               
                </View>

                {/* <View>
                <Text  style={{margin:5,justifyContent:"flex-end"}}>
                <Icon type="ionicon"  name={save[id] ? "bookmark" : "bookmark-outline"}
                color={save[id] ? "black" : "black"}
                onPress={() => this.bookmark(id)}
                 size={25} />
                 </Text>
                 </View> */}

                </View>
                <Text style={{padding:10,fontFamily:"Roboto-Regular"}}>{news.short_description}</Text>
              </View>
    
              {/* <View>
                <Icon type="ionicon" name="heart" />
              </View> */}
            </View>
        </View>)
       })
       
       return(
        <ScrollView style={{backgroundColor:'#f2f2f2'}}>
        {news}
        </ScrollView>
       )
     }
       
    }
  }
  
export default Post;  

//internal stylesheet 
const style=StyleSheet.create({
    
    card:{
      backgroundColor:"#fff",
      marginBottom:10,
      borderRadius:20
     
    },
    cardHeader:{
      flexDirection:"row",
      padding:10,
      justifyContent:"space-between",
      // backgroundColor:"red"
    },
    profileImage:{
        height:40,
        width:40,
        borderWidth:0.2 ,
        borderColor:"grey",
        borderRadius:50
    },
    name:{
      fontFamily:"Raleway-SemiBold",
      fontSize:15
    },
    type:{
      fontFamily:"Roboto-Regular",
      color:"grey"
    },
    postImage:{
      width:Dimensions.get("window").width,
      height:300

    },
    editIcon:{
        height:20,
        width:20
    },
    Icon:{
        height:28,
        width:28,
        marginRight:5
    }

})