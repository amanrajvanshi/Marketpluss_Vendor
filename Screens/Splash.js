import React, { Component } from 'react';
import {
    View,ImageBackground,
    StyleSheet,
    Image,Text,Dimensions,
} from 'react-native';
import {Icon} from "react-native-elements";

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Splash extends Component{
    componentDidMount(){
        setTimeout(()=>{
            this.props.navigation.navigate("MobileLogin")
        },2000)
    }
    render(){
        return(
            <View>
                <ImageBackground source={require('../img/background_Pic.png')} style={{height:"100%"}}>
                    <Image source={require('../img/logo/logo.png')} style={style.logo}/>

                    <View style={{}}>
                        <Text style={style.text}>
                            A Quality Product by Webixun Infoways.</Text>
                        
                        <View>
                        
                        <Text style={style.text}>Made With <Image source={require('../img/heart.png')} style={style.image}/>
                         <Text> in India.</Text></Text>
                        </View>
                       
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

export default Splash;

const style=StyleSheet.create({
    logo:{
        height:90,
        width:212,
        alignSelf:"center",
        top:220
    },
    text:{
        alignSelf:"center",
        top:450,
        fontFamily:"Roboto-SemiBold",
        alignContent:"flex-end"
    },
    image:{
        height:18,
        width:21   
    }
    
})