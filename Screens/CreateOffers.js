import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,TextInput,Pressable,
    ScrollView,Dimensions,TouchableOpacity
} from 'react-native';
import {Icon,Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import moment  from 'moment';
import DateTimePicker from "react-native-modal-datetime-picker";
import MultiSelect from 'react-native-multiple-select';

//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class CreateOffers extends Component{

    

    //for header left component
    renderLeftComponent(){
        return(
            <View style={{width:win.width,flexDirection:"row"}} >
                <Icon name="arrow-back-outline"  type="ionicon"
                onPress={()=>this.props.navigation.goBack()} style={{top:2.5}}/>
                <Text style={[styles.h3,{paddingLeft:15,bottom:1}]}>Create Offers</Text> 
            </View>
            )
    }

    render(){
        return(
            <View style={styles.container}>
                <View>
                    <Header 
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                            colors: ['#fff', '#fff'],
                        
                        
                        }}
                    />
                </View>

                <ScrollView>
                    <Fields navigation={this.props.navigation}/>
                </ScrollView>

            </View>

        )
    }
}

export default CreateOffers;



const items = [{
    id: '1',
    name: 'Oppo'
  }, {
    id: '2',
    name: 'Vivo'
  }, {
    id: '3',
    name: 'Micromax'
  }, {
    id: '4',
    name: 'Redmi'
  }, {
    id: '5',
    name: 'Realme'
  }, {
    id: '6',
    name: 'Samsung'
  }, {
    id: '7',
    name: 'Gionee'
  }, {
    id: '8',
    name: 'OnePluss'
  }, {
    id: '9',
    name: 'Nokia'
    }
];

class Fields extends Component{
    constructor(props){
        super(props);
        
        this.state={
            isVisible:false,
            chosenDate:"DD/MM/YYYY",
            selectedItems: [],
        }
    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
      };


    showPicker =()=>{
        // alert("hi");
        this.setState({
            isVisible:true
        })
    
    }

    handlePicker=(date)=>{
        if(this.state.chosenDate=="none"){
            this.setState({chosenDate:this.state.chosenDate})
        }
        this.setState({
            isVisible:false,
            chosenDate:moment(date).format('DD / MMMM/ YYYY')
        })
    }

    hidePicker =()=>{
        this.setState({
            isVisible:false
        })
    }

    render(){
        const { selectedItems } = this.state;
        return(
            <View style={{flex:1,marginBottom:15,borderTopWidth:1,borderColor:"#d3d3d3"}}>
                <View>
                    <Text style={style.fieldsTitle}>
                        Offer Title
                    </Text>
                    <TextInput 
                    style={style.textInput}/>
                </View>

                <View>
                    <Text style={style.fieldsTitle}>
                        Offer (%)
                    </Text>
                    <TextInput 
                    keyboardType="numeric"
                    style={style.textInput}/>
                </View>

                <Text style={style.fieldsTitle}>Product</Text>
                <View style={{marginLeft:30,marginRight:30}}>
                <MultiSelect
          hideTags
          items={items}
          uniqueKey="id"
          ref={(component) => { this.multiSelect = component }}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={selectedItems}
          selectText="Select"
          searchInputPlaceholderText="Search Items..."
          onChangeInput={ (text)=> console.log(text)}
          altFontFamily="ProximaNova-Light"
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
    
          tagTextColor="#CCC"
          selectedItemTextColor="green"
          selectedItemIconColor="green"
          itemTextColor="#000"
          displayKey="name"
          searchInputStyle={{ color: '#CCC' }}
          submitButtonColor="#326bf3"
          submitButtonText="Submit"
        />
        </View>

                <View>
                    <Text style={style.fieldsTitle}>
                        Market Price
                    </Text>
                    <TextInput 
                    keyboardType="numeric"
                    style={[style.textInput,{paddingLeft:30}]}/>
                    <Text style={{left:30, top:55,position:"absolute"}} >
                    <MaterialCommunityIcons name="currency-inr" size={20} />
                    </Text>
                </View>

                <View>
                    <Text style={style.fieldsTitle}>
                        Our Price
                    </Text>
                    <TextInput 
                    keyboardType="numeric"
                    style={[style.textInput,{paddingLeft:30}]}/>
                    <Text style={{left:30, top:55,position:"absolute"}} >
                    <MaterialCommunityIcons name="currency-inr" size={20} />
                    </Text>
                </View>

                <View>
                    <Text style={style.fieldsTitle}>
                        Description <Text style={{color:"grey"}}>(50words) </Text>
                    </Text>
                    <TextInput 
                    style={[style.textInput,{height:100}]}
                    multiline={true}
                    maxLength={100}/>
                </View>

                <View>
                    <Text style={style.fieldsTitle}>
                        Expiry date
                    </Text>
                    <View style={{borderBottomWidth:1,borderColor:"#d3d3d3",marginLeft:20,marginRight:30, paddingBottom:10}}>
                        <Pressable onPress={()=>this.showPicker()}>
                            <Text style={[styles.h4]}>
                        {this.state.chosenDate}
                    </Text>
                        </Pressable>
                        </View>
                </View>
                 {/* date picker */}
                 <DateTimePicker
                    isVisible={this.state.isVisible}
                    mode={"date"} 
                    is24Hour={false}
                    minimumDate={new Date()}
                    onConfirm={this.handlePicker}
                    onCancel={this.hidePicker}
                />

                <TouchableOpacity 
                onPress={()=>this.props.navigation.navigate("Offers")}
                style={style.buttonStyles}>
                <LinearGradient 
                    colors={['#326BF3', '#0b2564']}
                    style={styles.signIn}>

                    <Text style={[styles.textSignIn, {color:'#fff'}]}>
                    Create</Text>
                </LinearGradient>
                </TouchableOpacity>

            </View>
        )
    }
}

const style=StyleSheet.create({
    fieldsTitle:{
        fontFamily:"Raleway-Regular",
            fontSize:18,
            paddingLeft:20,
            padding:10,
    },
    textInput:{
      borderWidth: 1,
      borderColor:"#d3d3d3",

    //   backgroundColor: '#f5f5f5',
      borderRadius:5,
      padding:5,
      width:Dimensions.get("window").width/1.1,
      height: 40,
      alignContent: 'center',
      alignSelf: 'center',
      fontSize:15,
    },    buttonStyles:{
        width:"35%",
        alignSelf:"center",
        marginTop:50,
        marginRight:5,
        marginBottom:20
      }
})