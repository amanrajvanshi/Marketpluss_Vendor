import React, { Component } from 'react';
import {
  StyleSheet,Text,
  View,Dimensions,Image,Pressable,
  ScrollView,TouchableOpacity,TextInput
} from 'react-native';
import {Input,Header,Icon} from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');


class Profile extends Component{

    constructor(props){
        super(props);
    }

    //for header left component
    renderLeftComponent(){
        return(
        <View style={{width:win.width,flexDirection:"row"}} >
            <Icon name="arrow-back-outline"  type="ionicon"
            onPress={()=>this.props.navigation.goBack()} style={{top:3}}/>
            <Text style={[styles.h3,{paddingLeft:15}]}>Edit Profile</Text> 
        </View>
        )
    }

    render(){
        return(
            <View style={styles.container} >
                <ScrollView>
                <View>
                <Header 
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                        colors: ['#fff', '#fff'],
                    }}
                    />
                </View>
                <View style={{borderTopWidth:2,borderColor:"#f5f5f5",flex:1}}>
                <View style={style.container2}>
                  <View style={{paddingLeft:10,marginTop:10}}>
                      <Text style={style.fieldsText}>Name</Text>
                      <Input
                      inputContainerStyle={{
                        width:Dimensions.get("window").width/1.12,
                    }}/>
                  </View>

                  <View style={{paddingLeft:10}}>
                      <Text style={style.fieldsText}>Shop Name</Text>
                      <Input
                      inputContainerStyle={{
                        width:Dimensions.get("window").width/1.12,
                    }}/>
                  </View>

                  <View style={{paddingLeft:10}}>
                      <Text style={style.fieldsText}>Contact</Text>
                      <Input
                      inputContainerStyle={{
                        width:Dimensions.get("window").width/1.12,
                    }}
                    maxLength={10}
                    keyboardType="numeric"/>
                  </View>

                  <View style={{paddingLeft:10}}>
                      <Text style={style.fieldsText}>Email Address</Text>
                      <Input 
                      inputContainerStyle={{
                        width:Dimensions.get("window").width/1.12,
                    }}/>
                  </View>

                  <View style={{paddingLeft:10}}>
                      <Text style={style.fieldsText}>Shop Address</Text>
                      <Input 
                      inputContainerStyle={{
                        width:Dimensions.get("window").width/1.12,
                        }}/>
                  </View>

                  
                  
                  <TouchableOpacity  
            onPress={()=>this.props.navigation.navigate("Home")}
            style={style.buttonStyles}>
              <LinearGradient 
                colors={['#377ae3', '#0345b7']}
                style={styles.signIn}>

                <Text style={[styles.textSignIn, {color:'#fff'}]}>
                  Save</Text>
              
              </LinearGradient>
            </TouchableOpacity>
                </View>
            </View>
            </ScrollView>
            </View>
        )
    }
}

export default Profile;

const style=StyleSheet.create({
    container1:{
        backgroundColor:"#326bf3",
        width:"100%",
        height:230
    },
    container2:{
       
        width:"100%",
    },
    userImg:{
        height:120,
        width:120,
        alignSelf:"center",
        top:40,
        borderRadius:100,
        shadowRadius: 50,
        borderWidth:0.5,
        borderColor:"#fff",
        shadowOffset: { width: 100, height: 100},
    },
    editIcon:{
        position:"absolute",
        left:255,
        top:140
    },
    fieldsText:{
        fontSize:15,
        fontFamily:"Montserrat-SemiBold",
        color:"grey",
        marginLeft:10
    },
    GenderText:{
        fontSize:16,
        fontFamily:"Montserrat-SemiBold",
        color:"grey",
        paddingLeft:5
    },
    dobInput:{
        marginTop:15,
        fontSize:18,
        paddingLeft:10
    },
    buttonStyles:{
      width:"60%",
      alignSelf:"center",
      marginTop:35,
      marginRight:5
    },
    text:{
        fontFamily:"Raleway-SemiBold",
        fontSize:20,
        margin:5
    },

})