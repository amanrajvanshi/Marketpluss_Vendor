import React, { Component } from 'react';
import {
    View,ImageBackground,
    TouchableOpacity,
    StyleSheet,
    Image,ActivityIndicator
} from 'react-native';
import OTPTextView from 'react-native-otp-textinput';
import LinearGradient from 'react-native-linear-gradient';
import { Text } from 'react-native-elements';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

//Global StyleSheet Import
const style = require('../Components/Style.js');


class OtpVerify extends Component
{
  constructor(props)
    {
        super(props);

       this.state = {
        otpInput: '',
        inputText: '',isLoading: false
      }
    }

      updateOtpText = () => {
        // will automatically trigger handleOnTextChange callback passed
        this.input1.setValue(this.state.inputText);
      };

      mobile_verify = () =>
      {
          if(this.state.otpInput != null)
          {
            this.setState({ isLoading: true});
              var cc= this.props.route.params.contact_no;
 
              fetch('https://greenrabbit.in/hnn/public/api/otp-verification', {
                  method: 'POST',
                  headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                      contact:cc,
                      otp:this.state.otpInput
                           })
                  })
                  .then((response) => response.json())
                  .then((json) => {
 
                    if(json.msg=='ok')
                    {
                      if(json.user_type=='login')
                      {
                        try {
                          AsyncStorage.setItem('token',"Bearer "+json.token );
                          AsyncStorage.setItem('user',json.usr );
                          AsyncStorage.setItem('user_login',"yes" );
                        } catch (error) {
                         alert("nn")
                        }
                        
                      }
                      else{
 
                        try {
                          AsyncStorage.setItem('token',"Bearer "+json.token );
                          AsyncStorage.setItem('user',json.usr );
                        } catch (error) {
                         alert("nn")
                        }
                      
                        
                        this.props.navigation.navigate('FirstUserProfile');
                      }
                    }
                    else{
                      Toast.show("Invalid OTP, Try Again");
                    }
                    this.setState({isLoading:false});
                  })
                  .catch((error) => console.error(error))
                  .finally(() => {
                    this.setState({ isLoading: false });
                  });
          }
          else{
            alert("OTP requied");
          }
      }


    render()
    {
      
      // ()=>this.props.navigation.navigate('FirstUserProfile')
        return(
            <View style={style.container}>

              {/* header */}
              <View style={styles.header}>

               
                <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                {/* back icon */}
                <MaterialCommunityIcons name="arrow-left" size={30}
                onPress={()=>this.props.navigation.goBack()} style={styles.icon}/>

                {/* Logo */}
                <Image source={require("../img/logo/mp.png")} 
               style={{height:50,width:52,alignSelf:"flex-end",margin:10}}/>
                </View>

                {/* Image */}
                <Image source={require('../img/tick.png')} style={styles.image}/>

               
                
              </View>

              {/* Heading */}
              <Text style={[styles.heading,{color:'#ffbf0b'}]} >OTP 
              <Text style={styles.heading}> Verification</Text></Text>

              
              <Text style={styles.p} >Enter your OTP code we have sent to :</Text>
              <Text style={[styles.p,{top:-15}]}>+91 {this.props.route.params.paramKey}<Text style={{color:"#326bf3",fontFamily:"Raleway-Regular"}}onPress={()=>this.props.navigation.navigate('MobileLogin')} > Edit</Text></Text>


              {/* OTP TextInput */}
              <OTPTextView
                ref={(e) => (this.input1 = e)}
                containerStyle={styles.textInputContainer}
                handleTextChange={(text) => this.setState({otpInput: text})}
                inputCount={4}
                keyboardType="numeric"
              />
           
            {this.state.isLoading?
            <ActivityIndicator size="small" color="#222222" />
            :
            <TouchableOpacity  
            // onPress={this.mobile_verify}
            onPress={()=>this.props.navigation.navigate("CreateShopProfile")}
            style={styles.buttonStyles}>
              <LinearGradient 
                colors={['#326bf3', '#0b2654']}
                style={style.signIn}>

                <Text style={[style.textSignIn, {color:'#fff'}]}>
                  Verify & Proceed</Text>
              
              </LinearGradient>
            </TouchableOpacity>
    }
          </View>
          )
    }
}

export default OtpVerify;

//Internal StyleSheet
const styles = StyleSheet.create({
    textInputContainer: {
      marginBottom: 20,
      paddingLeft:50,
      paddingRight:50,
    },   
    image:{
        height:250,
        width:200,
        //marginTop:30,
        justifyContent:"center",
        alignSelf:"center"
    },
    icon:{
      // alignSelf:"center",
      // marginLeft:10,
      margin:15
    },
    heading:{
        color:"#1F449B",
        fontSize:24,
        fontFamily:"Montserrat-SemiBold",
        marginTop:10,
        alignSelf:"center"
    },
    p:
    {
      fontSize:14,
      fontFamily:"Montserrat-Regular",
      marginTop:25,
      alignSelf:"center"
    },
    buttonStyles:{
      width:"60%",
      alignSelf:"center",
      marginTop:25,
      marginRight:5
    }
  });