import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,Pressable,PermissionsAndroid, 
} from 'react-native';
import {Icon} from "react-native-elements"
import LinearGradient from 'react-native-linear-gradient';
import {Searchbar} from "react-native-paper";
import MapView, {Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Geolocation from '@react-native-community/geolocation';
navigator.geolocation = require('@react-native-community/geolocation');
//Global Style Import
const styles = require('../Components/Style.js');


class Location extends Component {
    render(){
        return(
            <GooglePlacesAutocomplete
            placeholder='Search'
            fetchDetails={true}
            returnKeyType={'default'}
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              console.log(data, details);
            }}
            query={{
              key: 'AIzaSyDG9RC60WCZTRhE2Du-BhOzrEgsCwckN7M',
              language: 'en',
            }}
          />
        )
    }
}

export default Location