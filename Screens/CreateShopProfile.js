import React, { Component } from 'react';
import {
    View,TouchableOpacity,
    StyleSheet,Text,
    Image,ActivityIndicator,ScrollView,Dimensions
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Icon,Input} from 'react-native-elements';
import RBSheet from 'react-native-raw-bottom-sheet';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from "react-native-image-crop-picker";

//Global StyleSheet Import
const styles = require('../Components/Style.js');


const options = {
    title: "Pick an Image",
    storageOptions:{
        skipBackup: true,
        path:'images'
    }
}
class CreateShopProfile extends Component{
    constructor(props){
        super(props);
    }

    camera =()=>{

        launchCamera(options, (response)=>{
            
            if(response.didCancel){
                console.warn(response)
                console.warn("User cancelled image picker");
            } else if (response.error){
                console.warn('ImagePicker Error: ', response.error);
            }else{
                // const source = {uri: response.assets.uri};
              let path = response.assets.map((path)=>{
                  return (
                      console.warn(path.uri)   
                  )
              })
              this.setState({image:path.uri})
  
                
                
            }
        })
      }
      gallery =()=>{
        ImagePicker.openPicker({
            width:300,
            height:400,
            cropping:true,
        }).then(image=>{
            console.log(image);
            this.setState({image:"Image Uploaded"})

        })
    }


    render(){
        return(
            <View style={styles.container}>
            <ScrollView>
                   {/* header */}
              <View style={styles.header}>
                <View>
                   {/* back icon */}
                   <MaterialCommunityIcons name="arrow-left" size={30}
                    onPress={()=>this.props.navigation.goBack()} style={style.icon}/>
                </View>

                {/* heading */}
                <Text style={[styles.heading,{marginLeft:20,marginTop:-5}]}>Create Shop Profile</Text>

                {/* View for image and icon */}
                <View style={{marginTop:15}}>

                <Image source={require("../img/dummyuser.jpg")} style={style.image}/>

                <Text style={style.editIcon}>
                <Icon type="ionicon" name="create-outline" 
                onPress={()=>this.RBSheet.open()}/>
                </Text>
                </View>

                {/* Bottom Sheet fot FAB */}
                <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={150}
                        customStyles={{
                            container:{
                                borderTopLeftRadius:20,
                                borderTopRightRadius:20
                            },
                        wrapper: {
                            // backgroundColor: "transparent",
                            borderWidth: 1
                        },
                        draggableIcon: {
                            backgroundColor: "grey"
                        }
                        }}
                    >
                        {/* bottom sheet elements */}
                    <View>
                        
                        {/* Bottom sheet View */}
                            <View style={{width:"100%",padding:20}}>
                            <TouchableOpacity onPress={this.camera}>
                                        <Text style={style.iconPencil}>
                                            <Icon name='camera' type="ionicon" color={'#0077c0'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Take a picture</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={this.gallery} > 
                                        <Text style={style.iconPencil}>
                                            <Icon name='folder' type="ionicon" color={'#0077c0'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Select from library</Text>
                                        </TouchableOpacity>

                            </View>
                        
                    </View>
                    </RBSheet>

                {/* View for fields */}
                <View>
                    <View style={{paddingLeft:10,marginTop:-10}}>
                        <Text style={style.fieldsText}>Name</Text>
                        <Input
                        maxLength={60}
                        style={{
                            fontSize:14,
                            color:"black"
                            }}
                        inputContainerStyle={{
                            width:Dimensions.get("window").width/1.12,
                        }}/>
                    </View>

                    <View style={{paddingLeft:10}}>
                        <Text style={style.fieldsText}> Shop Name</Text>
                        <Input
                        maxLength={60}
                        style={{
                            fontSize:14,
                            color:"black"
                            }}
                        inputContainerStyle={{
                            width:Dimensions.get("window").width/1.12,
                        }}/>
                    </View>

                    <View style={{paddingLeft:10}}>
                        <Text style={style.fieldsText}>Contact</Text>
                        <Input 
                        maxLength={10}
                        keyboardType="numeric"
                        style={{
                            fontSize:14,
                            color:"black"
                            }}
                        inputContainerStyle={{
                            width:Dimensions.get("window").width/1.12,
                        }}/>
                    </View>

                    <View style={{paddingLeft:10}}>
                        <Text style={style.fieldsText}>Email Address</Text>
                        <Input
                        maxLength={50}
                        style={{
                            fontSize:14,
                            color:"black"
                            }}
                        inputContainerStyle={{
                            width:Dimensions.get("window").width/1.12,
                        }}/>
                    </View>
                </View>

                 {/* Continue Button */}
                <TouchableOpacity
                onPress={()=>this.props.navigation.navigate("EnableLocation")}
                style={[styles.buttonStyles,{marginBottom:15,marginTop:-5}]}>
                    <LinearGradient 
                        colors={['#326bf3', '#0b2654']}
                        style={[styles.signIn]}>

                        <Text style={[styles.textSignIn, {
                        color:'#fff'}]}>Create</Text>
                        
                    </LinearGradient>
                </TouchableOpacity>

               </View>
            </ScrollView>
            </View>
        )
    }
}

export default CreateShopProfile;

const style=StyleSheet.create({
    icon:{
        margin:10
    },
    image:{
        height:100,
        width:100,
        borderRadius:100,
        borderWidth:1,
        borderColor:"#000",
        alignSelf:"center",
    },
    editIcon:{
        // position:"absolute",
        alignSelf:"center",
        left:40,
        top:-25
    },
    fieldsText:{
        fontSize:15,
        fontFamily:"Raleway-SemiBold",
        color:"grey",
        marginLeft:10
    },
    iconPencil:{
        marginLeft:20,
        fontSize:20,
        marginBottom:10
    },
    container1:{
        backgroundColor:"#326bf3",
        width:"100%",
        height:230
    },
    Text:{
        position:"absolute",
        fontSize:20,
        marginLeft:80,
        fontFamily:"Raleway-Medium"
    },
})