import React, { Component } from 'react';
import {
    Text,View,ScrollView,
    StyleSheet,Image,Pressable,
    TouchableOpacity,ImageBackground
} from 'react-native';
import {Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Demo from './Demo.js';
import RBSheet from 'react-native-raw-bottom-sheet';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from "react-native-image-crop-picker";

//Global StyleSheet Import
const styles = require('../Components/Style.js');


const options = {
    title: "Pick an Image",
    storageOptions:{
        skipBackup: true,
        path:'images'
    }
}

class Home extends Component{

    constructor(props){
        super(props);
    }

    camera =()=>{

        launchCamera(options, (response)=>{
            
            if(response.didCancel){
                console.warn(response)
                console.warn("User cancelled image picker");
            } else if (response.error){
                console.warn('ImagePicker Error: ', response.error);
            }else{
                // const source = {uri: response.assets.uri};
              let path = response.assets.map((path)=>{
                  return (
                      console.warn(path.uri)   
                  )
              })
              this.setState({image:path.uri})
  
                
                
            }
        })
      }
      gallery =()=>{
        ImagePicker.openPicker({
            width:300,
            height:400,
            cropping:true,
        }).then(image=>{
            console.log(image);
            this.setState({image:"Image Uploaded"})

        })
    }


  
    render(){
        return(
            <View style={[styles.container,{backgroundColor:"transparent"}]}>
         
            {/* View for Banner Image */}
            <View style={{backgroundColor:"black",flex:1}} >
                {/* Banner Image as background*/}
                <ImageBackground 
                imageStyle={{opacity:0.5}} source={require('../img/dummy1.jpg')} style={style.bannerImg}>
                
                {/* edit button */}
                <TouchableOpacity style={style.editIcon}
                onPress={()=>this.props.navigation.navigate("MultipleImage")}>
                    <Icon type="ionicon" name="camera" size={20} color="#000"/>
                    
                </TouchableOpacity>

                {/* View for profile image and name */}
                <View style={{padding:15,marginTop:70,marginBottom:15, flexDirection:"row"}}>
                    <Image source={require('../img/ab.jpg')} style={style.profileImg}/>
                    <Text onPress={()=>this.RBSheet.open()}  style={style.camIcon}>
                    <Icon type="ionicon" name="camera"  size={20} color="#000"/>
                    </Text>

                    
                        {/* Bottom Sheet for Camera */}
                        <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={150}
                        customStyles={{
                            container:{
                                borderTopLeftRadius:20,
                                borderTopRightRadius:20
                            },
                        wrapper: {
                            // backgroundColor: "transparent",
                            borderWidth: 1
                        },
                        draggableIcon: {
                            backgroundColor: "grey"
                        }
                        }}
                        >
                        {/* bottom sheet elements */}
                        <View>
                        
                        {/* Bottom sheet View */}
                            <View style={{width:"100%",padding:20}}>
                            <TouchableOpacity onPress={this.camera}>
                                        <Text style={style.iconPencil}>
                                            <Icon name='camera' type="ionicon" color={'#0077c0'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Take a picture</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={this.gallery} > 
                                        <Text style={style.iconPencil}>
                                            <Icon name='folder' type="ionicon" color={'#0077c0'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Select from library</Text>
                                        </TouchableOpacity>

                            </View>
                        
                        </View>
                        </RBSheet>
                    <View style={{flexDirection:"column",alignSelf:"center"}}>
                        {/* name text */}
                        <Text style={style.nameText}>
                            Hi, <Text style={{fontFamily:"Raleway-Bold",marginLeft:10}}>
                                 Aman
                            </Text>
                        </Text>
                        
                        <Text style={style.text}>
                            Welcome Back
                        </Text>
                    </View>
                </View>
                
            {/* View for main container */}
            <View style={style.mainContainer}>
                    <ScrollView>
                        <Demo/>
                        
                   </ScrollView>
            </View>
                </ImageBackground>

            </View>

          
           
            </View>
        )
    }
}



export default Home;

class Main extends Component{
    render(){
        return(
            <View>
              <Text>hi</Text>          
            </View>
        )
    }
}

const style=StyleSheet.create({
    bannerImg:{
        height:"100%",
        width:"100%",
        // marginTop:10
    },
    camIcon:{
        top:60,right:20,backgroundColor:"#dcdcdc",
    height:30,width:30,padding:5,alignContent:"center",
    borderRadius:30, justifyContent:"center"},

    editIcon:{
        position:"absolute",
        top:35,
        right:5,backgroundColor:"#dcdcdc",
        height:30,width:30,padding:5,alignContent:"center",
        borderRadius:30, justifyContent:"center"
    },
    iconPencil:{
        marginLeft:20,
        fontSize:20,
        marginBottom:10
    },
    Text:{
        position:"absolute",
        fontSize:20,
        marginLeft:80,
        fontFamily:"Raleway-Medium"
    },
    editText:{
        color:"#fff",
        fontSize:15,
        fontFamily:"Roboto-Regular",
        alignSelf:"center",
        marginRight:5,
    },
    profileImg:{
        height:85,
        width:85,
        borderRadius:100,
        // marginLeft:10
    },
    nameText:{
        color:'#fff',
        fontSize:25,
        // paddingLeft:0,
        fontFamily:"Raleway-Regular",
    },
    text:{
        color:'#fff',
        fontSize:15,
        // paddingLeft:10,
        fontFamily:"Roboto-Regular",
    },
    mainContainer:{
        backgroundColor:"#fff",
        flex:1,
        // position:"absolute",
        width:"100%",
        height:"100%",
        // marginTop:200,
        borderTopLeftRadius:30,
        borderTopRightRadius:30
    }
})