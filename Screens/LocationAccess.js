import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,Pressable,PermissionsAndroid, 
} from 'react-native';
import {Icon} from "react-native-elements"
import LinearGradient from 'react-native-linear-gradient';
import {Searchbar} from "react-native-paper";
import MapView, {Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Geolocation from '@react-native-community/geolocation';
navigator.geolocation = require('@react-native-community/geolocation');
//Global Style Import
const styles = require('../Components/Style.js');


const win = Dimensions.get('window');

const  requestLocationPermission = async () => {
  alert("hi")
  // try {
  //   const granted = await PermissionsAndroid.request(
  //     PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //     {
  //       title: "Cool Photo App Camera Permission",
  //       message:
  //         "Cool Photo App needs access to your camera " +
  //         "so you can take awesome pictures.",
  //       buttonNeutral: "Ask Me Later",
  //       buttonNegative: "Cancel",
  //       buttonPositive: "OK"
  //     }
  //   );
  //   if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //     console.log("You can use the camera");
  //   } else {
  //     console.log("Camera permission denied");
  //   }
  // } catch (err) {
  //   console.warn(err);
  // }
};

class LocationAccess extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={style.container}>
                {/* Component call for map */}
                <Map navigation={this.props.navigation}/>

                {/* Component call for bottom container */}
                {/* <Card navigation={this.props.navigation}/> */}
            </View>
        )
}
}
export default LocationAccess;

//this component is for map
class Map extends Component{
  constructor(props)
    {
        super(props);
        this.state={
          region:{
            latitude:30.3342968,
            longitude:78.053,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          },
            modalVisibleSorry:false,
            isbuloading:false,
            msg:'',
            timing:'',

            address:'',
            locality:"",
            postal_code:"",
            area:"",
            country:"",
            object:{}
        }
        
    }

    onRegionChange = region => {
      this.setState({
        region
      })
    }

    update_location = (data,details) =>
    {

      console.log(details);
      // alert(details.address_components[0].long_name)
      this.setState({address:details.address_components[0].long_name})
      this.setState({postal_code:details.address_components[1].long_name})
       this.setState({locality:details.address_components[3].long_name})
       this.setState({area:details.address_components[2].long_name})
       this.setState({country:details.address_components[4].long_name})
       this.setState({
         latitude:details.geometry.location.lat,
        longitude:details.geometry.location.lng,});
      }

       getAddressFromCoordinates({ latitude, longitude }) {
        return new Promise((resolve) => {
          const url = `https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?apiKey=${AIzaSyDG9RC60WCZTRhE2Du-BhOzrEgsCwckN7M}&mode=retrieveAddresses&prox=${latitude},${longitude}`

          // console.warn(json.data)
          console.warn(url)
          fetch(url)

            .then(res => res.json())
            .then((resJson) => {
              // the response had a deeply nested structure :/
              if (resJson
                && resJson.Response
                && resJson.Response.View
                && resJson.Response.View[0]
                && resJson.Response.View[0].Result
                && resJson.Response.View[0].Result[0]) {
                resolve(resJson.Response.View[0].Result[0].Location.Address.Label)
              } else {
                resolve()
              }
            })
            .catch((e) => {
              console.log('Error in getAddressFromCoordinates', e)
              resolve()
            })
        })
      }

      changeMarker =(corr)=>{
        // const { latLng } = corr;
        console.warn(corr);
        // console.warn(details,data)
        // const latitude=this.state.latitude;
        // const longitude=this.state.longitude;
        // const address=this.state.address
        // // alert(latitude)
        // this.setState({latitude,longitude,address})
      }

  render(){
    let {region}=this.state;
    return(
      <View style={{height:"100%"}}>
         <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={style.map} 
            initialRegion={region}
            onRegionChangeComplete={this.onRegionChange}
            >
              
            {/* <MapView.Marker
      draggable
      coordinate={{
      latitude: this.state.latitude,
      longitude: this.state.longitude}}
      title={this.state.address}
      description={this.state.locality}
      onDragEnd={(latitude,longitude)=>{this.getAddressFromCoordinates(latitude,longitude)}} /> */}
            {/* <Marker
            coordinate={{
              
              latitude: this.state.latitude,
              longitude: this.state.longitude}}>  
            <Icon name="location" color="red"  type="ionicon" size={35} style={{alignSelf:"center",position:"absolute"}} /> 
             </Marker> */}

          
         </MapView>
         
          <Text style={{position:"absolute",alignSelf:"center",top:150}}>
         <Icon name="location" color="red"  type="ionicon" size={35}  /> 
            </Text>

           <Pressable style={style.backIcon} 
            onPress={()=>this.props.navigation.goBack()}  >
            <Icon type="ionicon" name="arrow-back-outline" size={20} />
           </Pressable>

           <GooglePlacesAutocomplete
            placeholder='Search'
            fetchDetails={true}
            returnKeyType={'default'}
            onPress={(data, details = null) => {this.update_location(data,details)
            }}
            query={{
              key: 'AIzaSyDG9RC60WCZTRhE2Du-BhOzrEgsCwckN7M',
              language: 'en',
            }}
            styles={{
              container: {
                borderRadius:10,
                position:"absolute",
                top:55,
                width:"85%",
                alignSelf:"center",
                fontFamily:"Raleway-Regular",
                shadowColor: 'grey',
                shadowOpacity: 1.5,
                elevation: 2,
                shadowRadius: 10,
                shadowOffset: { width:1, height: 1 },
              }
            }}
          />

<View style={style.bottomContainer}>
          
          <Text style={style.select}>SELECT YOUR LOCATION </Text>
          <View style={{width:win.width, padding:10}}>
        <View style={{flexDirection:"row"}}>
         <Image source={require("../img/icons/pin1.png")} 
         style={{height:20,width:20,marginTop:5}}/>
         
         {/* change button row view */}
         <View style={{flexDirection:"row", width:"100%"}}>
    
         <Text numberOfLines={1} style={style.text}>{this.state.address}</Text>
         {/* <Pressable style={style.changeButton}>
             <Text style={style.changeText} >CHANGE</Text>
         </Pressable> */}
         </View>
    
         </View>
        <View>
         <Text 
         style={{fontFamily:"Raleway-Regular",fontSize:13}}
         >{this.state.postal_code} {this.state.area},{this.state.locality},{this.state.country}</Text>
        </View>
       
       
    
          <Pressable 
                 onPress={()=>this.props.navigation.navigate("LocationDetails",{address:this.state.address,
                  area:this.state.area,postal_code:this.state.postal_code,
                  locality:this.state.locality,
                  country:this.state.country},)}
                 style={style.confirmbutton}>
                 <LinearGradient 
                     colors={['#326BF3', '#0b2564']}
                     style={styles.signIn}>
    
                     <Text style={[styles.textSignIn, {color:'#fff'}]}>
                     CONFIRM LOCATION</Text>
                     
    
                 </LinearGradient>
                 </Pressable>
       </View>
             </View>
          {/* <View 
          style={{borderRadius:10
          ,position:"absolute",top:55,height:44,
          width:"85%",alignSelf:"center",
          fontFamily:"Raleway-Regular"}}>
            
            
          </View> */}
      

          {/* <Searchbar
          style={{borderRadius:10
          ,position:"absolute",top:55,height:44,
          width:"85%",justifyContent:"center",alignSelf:"center",
          fontFamily:"Raleway-Regular"}}
          placeholder="Search here..."
          icon={()=><Icon name="search-outline" type="ionicon"/>}
        /> */}
      </View>
    )
  }
}

// Internal styling 

const style = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:"transparent",
      height:"100%"
    },
    map: {
      height:"70%",
      // flex:0.7
    },      
      bottomContainer:{
        borderTopLeftRadius:15,
        borderTopRightRadius:15,
        backgroundColor:"white",
        position:"absolute",
        bottom:0,
        // flex:0.3,
        height:"30%"
      },
      text:{
        fontFamily:"Raleway-Bold",
        fontSize:20,
        marginLeft:5,
      },
      changeButton:{
      position:"absolute",
      right:20,
      borderWidth:0.4,
      borderRadius:7,
      backgroundColor:"#f3f3f3",

      },
      backIcon:{
          position:"absolute",
          top:10,
          backgroundColor:"white",
          borderRadius:50,
          left:10, 
          width:40,
          justifyContent:"center",
          height:40,
          padding:5
      },
      select:{
        fontSize:14,
        color:"grey",
        fontFamily:"Raleway-SemiBold",
        padding:10
      },
      confirmbutton:{
          width:"70%",
          alignSelf:"center",
          marginTop:15,
          marginRight:2,
          marginBottom:10
      },
      changeText:{
        color:"#1F449B", 
        padding:3.5,
        fontSize:13,
        fontFamily:"Roboto-SemiBold",
      }
   });