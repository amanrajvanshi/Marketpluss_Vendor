import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,Pressable,PermissionsAndroid, 
} from 'react-native';
import {Icon,Input} from "react-native-elements";
import Geolocation from '@react-native-community/geolocation';
import LinearGradient from 'react-native-linear-gradient';
import MapView, {Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class LocationDetails extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                <View>
                   {/* back icon */}
                   <MaterialCommunityIcons name="arrow-left" size={30}
                    onPress={()=>this.props.navigation.goBack()} style={{margin:10}}/>
                </View>
                </View>
               <ScrollView>
                   {/* Component call for second container */}
                   <Card navigation={this.props.navigation} data={this.props.route.params} />
               </ScrollView>
                
            </View>
        )
    }
}


export default LocationDetails;

class Card extends Component{
    constructor(props)
    {
        super(props);
        // console.warn(props);
    }
    render(){
        return(
            <View style={{height:"70%"}}>
                 {/* View for Location */}
                 <View style={{padding:10,marginLeft:20}}>
                        <View style={{flexDirection:"row",width:"70%",}}>
                        <Image source={require("../img/icons/pin1.png")} 
                        style={{height:20,width:20,marginTop:5}}/>
                        <Text style={style.text}>
                            {this.props.data.address}
                            </Text>
                        </View>
                    
                        <Text
                        style={{fontFamily:"Roboto-Regular",fontSize:12}}
                        >
                          {this.props.data.postal_code}  {this.props.data.area}, {this.props.data.locality},{this.props.data.country}
                            </Text>
                  </View>

                  {/* View for text container */}

                  <View style={style.container1}>
                      <Text style={[styles.small,{fontFamily:"Roboto-Regular",color:"grey",padding:2}]}>
                          A detailed address will help customers to {'\n'}
                        reach you.
                      </Text>
                  </View>

                  {/* View for fields */}
                  <View>
                      <Input 
                        placeholder="Shop No. / Block No. / Name"
                        style={{
                            fontSize:14,
                            color:"black"
                            }}
                        inputContainerStyle={{
                            width:Dimensions.get("window").width/1.17,
                            marginLeft:20,
                            marginRight:20
                        }}/>
                      <Input
                        placeholder="Lane / Road / Area (Optional)"
                        style={{
                            fontSize:14,
                            color:"black"
                            }}
                        inputContainerStyle={{
                            width:Dimensions.get("window").width/1.17,
                            marginLeft:20,
                            marginRight:20
                        }}/>
                      <Input
                        placeholder="City / State"
                        value={(this.props.data.locality) + ", " + (this.props.data.country)}
                        style={{
                            fontSize:14,
                            color:"black"
                            }}
                        inputContainerStyle={{
                            width:Dimensions.get("window").width/1.17,
                            marginLeft:20,
                            marginRight:20
                        }}/>
                  </View>

                  {/* Button */}
                  <View>
                    <TouchableOpacity  
                        onPress={()=>this.props.navigation.navigate("ChooseCategories")}
                        style={style.buttonStyles}>
                        <LinearGradient 
                            colors={['#326bf3', '#0b2654']}
                            style={styles.signIn}>

                            <Text style={[styles.textSignIn, {color:'#fff'}]}>
                            Confirm & Save</Text>
                        
                        </LinearGradient>
                    </TouchableOpacity>
                  </View>
            </View>
        )
    }
}

// Internal styling 
const style = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:"transparent",
      height:"100%"
    },
    map: {
      height:"100%",
      // flex:0.7
    },
    backIcon:{
      position:"absolute",
      top:10,
    //   backgroundColor:"white",
      borderRadius:50,
      left:10, 
      width:40,
      justifyContent:"center",
      height:40,
      padding:5
    },
    text:{
        fontFamily:"Roboto-Bold",
        fontSize:20,
        marginLeft:5,
    },
    container1:{
        borderRadius:5,
        borderWidth:1,
        borderColor:"#CCDDE4",
        backgroundColor:"#F1F4F8",
        margin:10,
        marginLeft:30,
        marginRight:30
    },
    buttonStyles:{
      width:"50%",
      alignSelf:"center",
      marginTop:25,
      marginRight:5
    }
})