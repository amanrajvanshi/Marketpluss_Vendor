import React, { Component } from 'react';
import {
    Text,View,ScrollView,Dimensions,
    StyleSheet,Image,Pressable,
    TouchableOpacity,ImageBackground
} from 'react-native';
import {Icon,Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Demo from './Demo.js';
import RBSheet from 'react-native-raw-bottom-sheet';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from "react-native-image-crop-picker";

//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');


const options = {
    title: "Pick an Image",
    storageOptions:{
        skipBackup: true,
        path:'images'
    }
}

class MultipleImage extends Component{
    //for header left component
 renderLeftComponent(){
    return(
    <View style={{width:win.width,flexDirection:"row"}} >
        <Icon name="arrow-back-outline"  type="ionicon"
        onPress={()=>this.props.navigation.goBack()} style={{top:2.5}}/>
        <Text style={[styles.h3,{paddingLeft:15,bottom:1}]}>Select images</Text> 
    </View>
    )
}



camera =()=>{

    launchCamera(options, (response)=>{
        
        if(response.didCancel){
            console.warn(response)
            console.warn("User cancelled image picker");
        } else if (response.error){
            console.warn('ImagePicker Error: ', response.error);
        }else{
            // const source = {uri: response.assets.uri};
          let path = response.assets.map((path)=>{
              return (
                  console.warn(path.uri)   
              )
          })
          this.setState({image:path.uri})

            
            
        }
    })
  }
  gallery =()=>{
    ImagePicker.openPicker({
        width:300,
        height:400,
        cropping:true,
    }).then(image=>{
        console.log(image);
        this.setState({image:"Image Uploaded"})

    })
}

    render(){
        return(
            <View style={styles.container}>
                    <View>
                <Header 
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                        colors: ['#fff', '#fff'],
                    }}
                />
            </View>
                    <View style={{flexDirection:"row"}}>
                        <TouchableOpacity  onPress={()=>this.RBSheet.open()} style={{height:110,width:"30%",margin:5,alignContent:"center",justifyContent:"center", backgroundColor:"#f5f5f5"}}>
                        <Image source={require('../img/abcd.png')}
                                style={style.serviceImg}/>
                        </TouchableOpacity>

                        <TouchableOpacity  onPress={()=>this.RBSheet.open()} style={{height:110,width:"30%",margin:5,alignContent:"center",justifyContent:"center", backgroundColor:"#f5f5f5"}}>
                        <Image source={require('../img/abcd.png')}
                                style={style.serviceImg}/>
                        </TouchableOpacity>

                        <TouchableOpacity  onPress={()=>this.RBSheet.open()} style={{height:110,width:"30%",margin:5,alignContent:"center",justifyContent:"center", backgroundColor:"#f5f5f5"}}>
                        <Image source={require('../img/abcd.png')}
                                style={style.serviceImg}/>
                        </TouchableOpacity>


                    </View>

                    <View style={{flexDirection:"row"}}>
                        <TouchableOpacity  onPress={()=>this.RBSheet.open()} style={{height:110,width:"30%",margin:5,alignContent:"center",justifyContent:"center", backgroundColor:"#f5f5f5"}}>
                        <Image source={require('../img/abcd.png')}
                                style={style.serviceImg}/>
                        </TouchableOpacity>

                        <TouchableOpacity  onPress={()=>this.RBSheet.open()} style={{height:110,width:"30%",margin:5,alignContent:"center",justifyContent:"center", backgroundColor:"#f5f5f5"}}>
                        <Image source={require('../img/abcd.png')}
                                style={style.serviceImg}/>
                        </TouchableOpacity>

                        <TouchableOpacity  onPress={()=>this.RBSheet.open()} style={{height:110,width:"30%",margin:5,alignContent:"center",justifyContent:"center", backgroundColor:"#f5f5f5"}}>
                        <Image source={require('../img/abcd.png')}
                                style={style.serviceImg}/>
                        </TouchableOpacity>


                    </View>


                          {/* Bottom Sheet for Camera */}
                          <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={150}
                        customStyles={{
                            container:{
                                borderTopLeftRadius:20,
                                borderTopRightRadius:20
                            },
                        wrapper: {
                            // backgroundColor: "transparent",
                            borderWidth: 1
                        },
                        draggableIcon: {
                            backgroundColor: "grey"
                        }
                        }}
                        >
                        {/* bottom sheet elements */}
                        <View>
                        
                        {/* Bottom sheet View */}
                            <View style={{width:"100%",padding:20}}>
                            <TouchableOpacity onPress={this.camera}>
                                        <Text style={style.iconPencil}>
                                            <Icon name='camera' type="ionicon" color={'#0077c0'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Take a picture</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={this.gallery} > 
                                        <Text style={style.iconPencil}>
                                            <Icon name='folder' type="ionicon" color={'#0077c0'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Select from library</Text>
                                        </TouchableOpacity>
                            </View>
                        
                        </View>
                        </RBSheet>
                        
                {/* Button */}
                <Pressable 
                onPress={()=>this.props.navigation.navigate("Home")}
                style={style.buttonStyles}>
                <LinearGradient 
                    colors={['#326BF3', '#0b2564']}
                    style={styles.signIn}>

                    <Text style={[styles.textSignIn, {color:'#fff'}]}>
                    Done</Text>
                </LinearGradient>
                </Pressable>
            </View>
        )
    }
}

export default MultipleImage


const style=StyleSheet.create({
    serviceImg:{
        height:80,
        width:120,
        // borderWidth:0.2,
        borderColor:"#000",
        alignSelf:"center",
        // marginLeft:30,
    },
    iconPencil:{
        marginLeft:20,
        fontSize:20,
        marginBottom:10
    },
    Text:{
        position:"absolute",
        fontSize:20,
        marginLeft:80,
        fontFamily:"Raleway-Medium"
    },
    buttonStyles:{
        width:"70%",
        alignSelf:"center",
        marginTop:50,
        marginRight:5
      },
})