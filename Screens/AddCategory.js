import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,TextInput,
    ScrollView,Dimensions,TouchableOpacity
} from 'react-native';
import {Icon,Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
// import DropDownPicker from 'react-native-dropdown-picker';
import RBSheet from 'react-native-raw-bottom-sheet';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from "react-native-image-crop-picker";
import {Picker} from '@react-native-picker/picker';
//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class AddCategory extends Component{
        //for header left component
        renderLeftComponent(){
            return(
                <View style={{width:win.width,flexDirection:"row",paddingBottom:5,}} >
                    <Icon name="arrow-back-outline"  type="ionicon"
                    onPress={()=>this.props.navigation.goBack()} style={{top:2.5}}/>
                    <Text style={[styles.h3,{paddingLeft:15,bottom:1}]}>Add Category</Text> 
                </View>
                )
        }
    
    render(){
        return(
            <View style={styles.container}>
                    <View>
                    <Header 
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                            colors: ['#fff', '#fff'],
                        
                        
                        }}
                    />
                </View>
                <View style={{flex:1,marginBottom:15,borderTopWidth:1,borderColor:"#d3d3d3"}}>
                <View>
                    <Text style={style.fieldsTitle}>
                      Category  Name
                    </Text>
                    <TextInput 
                    style={style.textInput}/>
                </View>
                <View>
                    <Text style={style.fieldsTitle}>
                      Sub-Category
                    </Text>
                    <TextInput 
                    style={style.textInput}/>
                </View>
                <TouchableOpacity style={style.uploadButton} onPress={()=>this.props.navigation.goBack()} >
                                    <Text style={style.buttonText}>
                                        Add
                                    </Text>
                                </TouchableOpacity>
                </View>
                </View>
        )
    }
}

export default AddCategory

const style=StyleSheet.create({
    fieldsTitle:{
        fontFamily:"Raleway-Regular",
        // color:"grey",
        fontSize:18,
        padding:10,
        paddingLeft:30
        
    },
    textInput:{
        borderWidth: 1,
        borderColor:"#d3d3d3",
  
      //   backgroundColor: '#f5f5f5',
        borderRadius:5,
        padding:5,
        width:Dimensions.get("window").width/1.1,
        height: 40,
        alignContent: 'center',
        alignSelf: 'center',
        fontSize:15,
    },
    uploadButton:{
        backgroundColor:"#326bf3",
        width:105,
        height:40,
        justifyContent:"center",
        padding:5,
        borderRadius:5,
        alignSelf:"center",
        alignItems:"center",
        // marginLeft:20,
        marginTop:20
    },
    buttonText:{
        fontFamily:"Raleway-SemiBold",
        color:"#fff",
        fontSize:18
    }

})