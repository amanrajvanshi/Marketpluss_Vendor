import React, { Component } from 'react';
import {
    Text,View,ScrollView,
    StyleSheet,Image,Pressable,
    TouchableOpacity,ImageBackground
} from 'react-native';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"

//Global StyleSheet Import
const styles = require('../Components/Style.js');

class VerificationDone extends Component{

    constructor(props){
        super(props);
    }

    componentDidMount(){
        setTimeout(()=>{
            this.props.navigation.navigate("Home")
        },2000)
    }

  
    render(){
        return(
            <View style={styles.container}>

                {/* header back icon */}
                <MaterialCommunityIcons name="arrow-left" size={30}
                onPress={()=>this.props.navigation.goBack()} style={style.icon}/>

                {/* View for content */}
                <View>
                <Text style={[styles.h2,{alignSelf:"center",marginTop:50}]}>Verification Complete!</Text>
                <Image source={require("../img/icons/check.png")} style={style.image}/>
                <Text style={[styles.h4,{color:"#326bf3",marginTop:80,alignSelf:"center"}]}>Congratulations You Have </Text>
                <Text style={[styles.h4,{color:"#326bf3",alignSelf:"center"}]}
                >Completed Your Profile.</Text>
                </View>
            
            </View>
        )
    }
}

export default VerificationDone;

const style=StyleSheet.create({
    icon:{
        margin:10
    },
    image:{
        height:100,
        width:100,
        alignSelf:"center",
        marginTop:50
    }
})