import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import {
    View,ImageBackground,Alert,
    StyleSheet,Pressable,Switch,
    Image,Text,Dimensions
} from 'react-native';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import {Header,Icon} from 'react-native-elements'
import { ScrollView } from 'react-native-gesture-handler';
import RBSheet from "react-native-raw-bottom-sheet";
import Toast from 'react-native-simple-toast';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');


class Services extends Component{

    constructor(props){
        super(props);
        
    }

    render(){
        return(
            <View style={[styles.container,]}>
                            
                  <ScrollView style={{flex:1}}>
                        <Card navigation={this.props.navigation} />
                        <Card navigation={this.props.navigation} />
                        <Card navigation={this.props.navigation} />
                        <Card navigation={this.props.navigation} />
                        <Card navigation={this.props.navigation} />
                        <Card navigation={this.props.navigation} />
                  </ScrollView>
                 
               {/* fab button */}
                <View>
                <TouchableOpacity style={style.fab}
                onPress={()=>this.props.navigation.navigate("CreateService")}>
                        <Icon name="add-outline" color="#fff" size={25} type="ionicon" style={{alignSelf:"center"}}/>
                </TouchableOpacity>
                </View>
               
            </View>

    )
}
}

export default Services;



class Card extends Component{
    constructor(props){
        super(props);
        this.state={
            isOn:false
        }
    }
    
    toggle=()=>{
        if(this.state.isOn==false){
            this.setState({isOn:true})
        }
        else{
            this.setState({isOn:false})
        }
    }

    alertFunc=()=>{
        Alert.alert(
          "Are you sure?",
          "Delete this Service",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => Toast.show("Deleted Successfully") }
          ]
        )
      }
  

    render(){
        return(
            <View style={style.card}>

                   <View style={{flexDirection:"row",width:"100%" }}>
                      {/* View for Image */}
                       <View style={{width:"27%"}}>
                        <Image source={require("../img/download.jpg")}
                        style={style.logo}/>
                        </View>
                        {/* View for Content */}
                        
                        <View style={style.contentView}>
                            {/* View for name and heart */}
                            <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                                 {/* Text View */}
                                <View style={{width:170,}}>
                        <Text style={[styles.smallHeading,{top:10,}]}>
                            Bbq Chicken (Half)
                            </Text>
                        <Text numberOfLines={3} style={[styles.p,{top:5,fontSize:12, }]}> 
                        Freshly Marinated Chicken Barbequed with spiced and
                        butter.
                         </Text>
                        </View>
                        {/* View for toggle icon  */}
                        <View style={{margin:5,marginTop:10, flexDirection:"row"}}>
                            <View >
                            <Switch 
                            trackColor={{false: "#d3d3d3", true : "#326bf3"}}
                            thumbColor={this.state.isOn ? "white" : "white"}
                            value={this.state.isOn}
                            onValueChange={()=>this.toggle()}
                            />
                            </View>
                            
                            <Icon type="ionicon" name="ellipsis-vertical"  onPress={()=>this.RBSheet.open()}  size={22} />
                        </View>
                        </View>

                          {/* Bottom Sheet for edit or delete options */}

          <RBSheet
                            ref={ref=>{this.RBSheet = ref;}}
                            closeOnDragDown={true}
                            closeOnPressMask={true}
                            height={170}
                            customStyles={{
                                container: {
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                  },
                            draggableIcon: {
                                backgroundColor: ""
                            }
                            }}
                        >
                            {/* bottom sheet elements */}
                        <View >
                            {/* new container search view */}
                                <View>
                                    {/* to share */}
                                    <View style={{flexDirection:"row",padding:10}}>
                                    <TouchableOpacity style={{flexDirection:"row"}} 
                                    onPress={()=>this.props.navigation.navigate("CreateService")}>
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,alignItems:"center",justifyContent:"center", borderRadius:50}}>
                                        <Icon type="ionicon" name="create-outline"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}>
                                        Edit</Text>
                                        </TouchableOpacity>
                                    </View>

                                    {/* to report */}
                                    <View style={{flexDirection:"row",padding:10}}>
                                     

                                        <TouchableOpacity style={{flexDirection:"row"}} onPress={()=>this.alertFunc()
                                          }>
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,justifyContent:"center",borderRadius:50}} >
                                        <Icon type="ionicon" name="trash-bin"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}
                                        >Delete</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
        
       
           
                        </View>
                        </RBSheet>
         



                        {/* View for Price and offer */}
                        <View style={{flexDirection:"row",justifyContent:"space-between",alignSelf:"flex-end", marginTop:8 }}>
                            <View style={{flexDirection:"row"}}>
                        <Text style={[styles.p,{fontFamily:"Roboto-Regular",color:"grey" ,textDecorationLine: 'line-through',
                        textDecorationStyle: 'solid'}]}>
                            300/-
                            </Text>
                            <Text style={[styles.p,{marginLeft:10, fontFamily:"Roboto-Bold"}]}>
                            200/-
                            </Text>
                            </View>

                            {/* <View>
                        <TouchableOpacity style={[style.viewDetailsButton,{flexDirection:"row"}]}
                        // onPress={()=>this.props.navigation.navigate("ShopOffers")}
                        >
                            
                            <MaterialCommunityIcons name="sale"  size={22} />
                            <Text style={style.textButton}>
                                Offers
                            </Text>
                        </TouchableOpacity>
                        </View> */}
                            </View>

                        </View>
                        
                   </View>
               </View>
        )
    }
}

const style=StyleSheet.create({
    header:{
        width:Dimensions.get("window").width/2-40,
        height:50,
        backgroundColor:"#fff",
        justifyContent:"center",
        borderColor:"black"
    },
    headerText:{
        fontSize:20,
        borderColor:"black",
        color:"black",
        alignSelf:"center",
        fontFamily:"Raleway-SemiBold",
    },
    text:{
        fontFamily:"Raleway-SemiBold",
        fontSize:18,
        margin:10
    },
    card:{
        backgroundColor:"#fff",
        alignSelf:"center",
        width:Dimensions.get("window").width/1.05,
        top:5,
        marginBottom:10,
        shadowRadius: 50,
        shadowOffset: { width: 50, height: 50 },
        elevation:2,
        borderRadius:15,
        padding:6
    },
    logo:{
        height:90,
        width:"95%",
        // borderWidth:0.2,
        // borderRadius:10,
        borderColor:"black",
        margin:10,
        marginLeft:10
    },
    viewDetailsButton:{
        borderColor:"#000",
        height:35,
        flexDirection:"row",
        justifyContent:"space-evenly",
        width:110,
        alignContent:"center",
        alignItems:"center",
        alignSelf:"flex-end",
        borderRadius:10,
        // position:"absolute",
        // top:80,
        // left:165
        //alignSelf:"flex-end"
    },
    textButton:{
        fontFamily:"Raleway-SemiBold",
        fontSize:15,
        color:"#000",
        marginLeft:-10

    },iconView:{
        width:32,
        height:32,
        shadowColor: '#fafafa',
        shadowOpacity: 1,
        elevation: 1,
        padding:6,
        shadowRadius: 2,
        shadowOffset: { width:1, height: 1 },
        alignContent:"center",
        alignItems:"center",
        justifyContent:"center",
        borderRadius:100
    },

    contentView:{
        flexDirection:"column",
        width:"68%",
        marginRight:10,
        // paddingBottom:10, 
        // borderBottomWidth:0.5,
        // borderColor:"#d3d3d3",
         marginLeft:10,
        //  marginTop:10,
        
       },
       fab:{
        backgroundColor:"#326bf3",
        borderRadius:100,
        height:50,
        width:50,
        bottom:10,
        right:10,
        // alignSelf:"flex-end",
        // margin:20,
        justifyContent:"center",
        position:"absolute"
    }
})