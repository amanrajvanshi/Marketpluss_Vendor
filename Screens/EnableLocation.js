import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,
    TouchableOpacity,Pressable
} from 'react-native';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import LinearGradient from 'react-native-linear-gradient';

//Global Style Import
const styles = require('../Components/Style.js');

class EnableLocation extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={styles.container}>
                
                <View style={styles.header}>
                <View>
                   {/* back icon */}
                   <MaterialCommunityIcons name="arrow-left" size={30}
                    onPress={()=>this.props.navigation.goBack()} style={style.icon}/>
                </View>
                </View>
                {/* Image */}
                <View>
                    <Image source={require("../img/brochure.png")} style={style.image}/>
                </View>

                {/* Text */}
                <View style={{marginTop:30}}>
                <Text style={[style.heading,{color:"#1F449B",fontSize:24}]}>
                <Text style={[style.heading,{color:"#ffbf0b"}]}>Allow
                </Text> your location</Text>
                <Text style={style.text}>We need to enable your</Text>
                <Text style={style.text}>location services for this.</Text>
                </View>

                <View>

                {/* Button */}
                <Pressable 
                onPress={()=>this.props.navigation.navigate("LocationAccess")}
                style={style.buttonStyles}>
                <LinearGradient 
                    colors={['#326BF3', '#0b2564']}
                    style={styles.signIn}>

                    <Text style={[styles.textSignIn, {color:'#fff'}]}>
                    Sure I'd like that</Text>
                </LinearGradient>
                </Pressable>

                {/* Not now Button */}
                <TouchableOpacity style={style.notNowButton}
                onPress={()=>this.props.navigation.navigate("ChooseCategories")}>
                    <Text style={style.notNowButtonText}>Not Now</Text>
                </TouchableOpacity>
                </View>

            </View>
        )
    }
}

export default EnableLocation;

//internal style
const style=StyleSheet.create({
      icon:{
          margin:10
      },
      heading:{
          color:"#1F449B",
          fontSize:24,
          fontFamily:"Raleway-Bold",
          marginTop:45,
          alignSelf:"center"
      },
      p:
      {
        fontSize:14,
        fontFamily:"Raleway-Regular",
        marginTop:25,
        alignSelf:"center",
        color:"#1F449B",
      },
      image:{
          height:250,
          width:250,
          justifyContent:"center",
          alignSelf:"center",
          marginTop:10
      },
      buttonStyles:{
        width:"70%",
        alignSelf:"center",
        marginTop:50,
        marginRight:5
      },
      heading:{
          fontSize:25,
          fontFamily:"Raleway-SemiBold",
          justifyContent:"center",
          alignSelf:"center",
          marginBottom:10
      },
      text: {
          color: 'grey',
          fontSize: 15,
          fontFamily:"Raleway-SemiBold",
          justifyContent:"center",
          alignSelf:"center",
      },
      notNowButton:{
          alignSelf:"center",
          marginTop:20
      },
      notNowButtonText:{
        fontSize:18,
        fontFamily:"Roboto-SemiBold",
        color:"#1F449B",
      }

})