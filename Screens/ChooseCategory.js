import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,Pressable,PermissionsAndroid, 
} from 'react-native';
import {Icon,Input} from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import { ScrollView } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import CategoriesSelect from '../Components/CategoriesSelect';

//Global StyleSheet Import
const styles = require('../Components/Style.js');

class ChooseCategories extends Component{
    render(){
        return(
            <View style={styles.container}>
                
                <ScrollView>
                    <View style={styles.header}>
                        <View>
                        {/* back icon */}
                        <MaterialCommunityIcons name="arrow-left" size={30}
                            onPress={()=>this.props.navigation.goBack()} style={style.icon}/>
                        </View>

                        
                        {/* heading */}
                        <Text style={[styles.heading,{marginLeft:20,marginTop:-5}]}>Choose</Text>
                        <Text style={[styles.h3,{marginLeft:20}]}>Your Favorite Categories</Text>

                        {/* Component for Categories select */}
                        <CategoriesSelect/>
                        
                        <View>
                        <TouchableOpacity  
                        onPress={()=>this.props.navigation.navigate("UnderVerification")}
                        style={style.buttonStyles}>
                        <LinearGradient 
                            colors={['#326bf3', '#0b2654']}
                            style={styles.signIn}>

                            <Text style={[styles.textSignIn, {color:'#fff'}]}>
                            Submit</Text>
                        
                        </LinearGradient>
                        </TouchableOpacity>
                        </View>

                    </View>
                </ScrollView>

                
            </View>
        )
    }
}

export default ChooseCategories;

const style=StyleSheet.create({
    icon:{
        margin:10
    },
    buttonStyles:{
        width:"40%",
        alignSelf:"center",
        marginTop:25,
        marginRight:5,
        marginBottom:10
    }
})