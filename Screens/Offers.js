import React, { Component } from 'react';
import {
    Text,View,ScrollView,Switch,Alert,
    StyleSheet,Image,
    TouchableOpacity,Dimensions, ImageBackground
} from 'react-native';
import {Icon,Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from "react-native-raw-bottom-sheet";
import Toast from 'react-native-simple-toast';


//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Offers extends Component{

    constructor(props){
        super(props);
        this.state={
            isOn:false
        }
    }

    //for header left component
    renderLeftComponent(){
        return(
        <View style={{width:win.width}} >
            <Text style={[styles.h3]}>Offers</Text> 
        </View>
        )
    }
  
    render(){
        return(
            <View style={styles.container}>

                <View>
                <Header 
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                        colors: ['#fff', '#fff'],
                    }}
                    />
                </View>

                <ScrollView>
                    {/* Component call for image card */}
                    <ImageCard/>

                     {/* Component call for offers card */}
                     <OffersCard navigation={this.props.navigation}/>
                     <OffersCard navigation={this.props.navigation}/>
                     <OffersCard navigation={this.props.navigation}/>
                     <OffersCard navigation={this.props.navigation}/>
                     <OffersCard navigation={this.props.navigation}/>
                     <OffersCard navigation={this.props.navigation}/>
                     <OffersCard navigation={this.props.navigation}/>
                </ScrollView>
            
            {/* fab button */}
          <View>
          <TouchableOpacity style={style.fab}
          onPress={()=>this.props.navigation.navigate("CreateOffers")}>
                <Icon name="add-outline" color="#fff" size={25} type="ionicon" style={{alignSelf:"center"}}/>
          </TouchableOpacity>
          </View>
            </View>
        )
    }
}

export default Offers;

//Offers Image 
class ImageCard extends Component{
    render(){
        return(
            <View>
                <ImageBackground source={require("../img/background_Pic.png")} style={{width:"100%"}} >
                <Image source={require("../img/sff.png")} style={style.image}/>
                </ImageBackground>
                {/* <View style={{marginTop:20}}>
                
                <Text style={[styles.h3,{fontFamily:"Raleway-Bold",marginLeft:10}]}>
                    Product Wise Offers</Text>
                    </View> */}
            </View>
        )
    }
}

//Card for Products offers
class OffersCard extends Component{
    constructor(props){
        super(props);
        this.state={
            isOn:false
        }
    }

    toggle=()=>{
        if(this.state.isOn==false){
            this.setState({isOn:true})
        }
        else{
            this.setState({isOn:false})
        }
    }
    alertFunc=()=>{
        Alert.alert(
          "Are you sure?",
          "Delete this Offer",
          [
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            },
            { text: "OK", onPress: () => Toast.show("Deleted Successfully") }
          ]
        )
      }
  
    render(){
     
        return(
            <View>
            {/* View For Card */}
            <View style={style.card}>

            <View style={{flexDirection:"row",width:"100%", }}>
               {/* View for Image */}
                <View style={{width:"27%"}}>
                 <Image source={require("../img/image.jpg")}
                 style={style.logo}/>

                 {/* View for %Off */}
                 <View style={{width:"85%",backgroundColor:"#326bf3",height:30,top:85,left:15,position:"absolute", alignItems:"center",borderRadius:5}} >
                                {/* <Text style={[styles.headingSmall,{
                                    color:"#fff",fontFamily:"Montserrat-Regular"
                                }]}>-Flat Deal-</Text> */}
                                <Text style={{fontFamily:"Montserrat-Bold",
                            fontSize:18,color:"#fff"}}>
                                    20% Off</Text>
                    </View>
                </View>

             
                 {/* View for Content */}
                 
                 <View style={style.contentView}>
                     {/* View for name and heart */}
                    <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                          {/* Text View */}
                         <View style={{width:170}} >
                             {/* types of cuisine heading */}
                            <Text numberOfLines={1} style={[styles.headingSmall,{top:10,marginLeft:5}]}>
                            North Indian | Desserts
                            </Text>

                            {/* type heading */}
                            <Text numberOfLines={1} style={[styles.small,{top:10,fontSize:15,
                                marginLeft:0,color:"#000",
                                marginLeft:5,fontFamily:"Raleway-Bold"}]}> 
                            South Indian Breakfast</Text>
                            
                            {/* description */}
                            <Text numberOfLines={3} style={[styles.small,{top:10,fontSize:12,
                                marginLeft:5}]}> 
                            Freshly made coconut based light and healthy
                            breakfast full of protein and vitamins.</Text>
                        </View>
                        {/* View for toggle icon  */}
                        <View style={{margin:5,marginTop:10, flexDirection:"row"}}>
                            <View >
                            <Switch 
                            trackColor={{false: "#d3d3d3", true : "#326bf3"}}
                            thumbColor={this.state.isOn ? "white" : "white"}
                            value={this.state.isOn}
                            onValueChange={()=>this.toggle()}
                            />
                            </View>
                            
                            <Icon type="ionicon" name="ellipsis-vertical" onPress={()=>this.RBSheet.open()}  size={22} />
                        </View>
                        </View>

                          {/* Bottom Sheet for edit or delete options */}

          <RBSheet
                            ref={ref=>{this.RBSheet = ref;}}
                            closeOnDragDown={true}
                            closeOnPressMask={true}
                            height={170}
                            customStyles={{
                                container: {
                                    borderTopRightRadius: 20,
                                    borderTopLeftRadius: 20,
                                  },
                            draggableIcon: {
                                backgroundColor: ""
                            }
                            }}
                        >
                            {/* bottom sheet elements */}
                        <View >
                            {/* new container search view */}
                                <View>
                                    {/* to share */}
                                    <View style={{flexDirection:"row",padding:10}}>
                                    <TouchableOpacity style={{flexDirection:"row"}} 
                                    onPress={()=>this.props.navigation.navigate("CreateOffers")}>
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,alignItems:"center",justifyContent:"center", borderRadius:50}}>
                                        <Icon type="ionicon" name="create-outline"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}>
                                        Edit</Text>
                                        </TouchableOpacity>
                                    </View>

                                    {/* to report */}
                                    <View style={{flexDirection:"row",padding:10}}>
                                     

                                        <TouchableOpacity style={{flexDirection:"row"}} onPress={()=>this.alertFunc()
                                          }>
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,justifyContent:"center",borderRadius:50}} >
                                        <Icon type="ionicon" name="trash-bin"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}
                                        >Delete</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
        
       
           
                        </View>
                        </RBSheet>
                    {/* View for Price and offer */}
                    <View style={{flexDirection:"row",justifyContent:"flex-end", marginTop:8 }}>
                        
                        <View style={{flexDirection:"row"}}>
                        <Text style={[styles.p,{fontFamily:"Roboto-Regular",color:"grey", textDecorationLine: 'line-through',
                            textDecorationStyle: 'solid'}]}>
                             ₹ 240/-
                        </Text>
                        <Text style={[styles.p,{marginLeft:10, fontFamily:"Roboto-Bold"}]}>
                        ₹ 200/-
                        </Text>
                        </View>
                        
                    </View>
                     
                        

                </View>
                 
            </View>
        </View>
        </View>
    
        )
    }
}

const style=StyleSheet.create({
    icon:{
        margin:10
    },
    image:{
        height:200,
        width:250,
        alignSelf:"center",
    },
    card:{
        // backgroundColor:"green",
        backgroundColor:"#FBF9F9",
        alignSelf:"center",
        width:Dimensions.get("window").width/1.03,
        top:10,
        marginBottom:10,
        shadowRadius: 50,
        shadowOffset: { width: 50, height: 50 },
        
    },
    logo:{
        height:90,
        width:"95%",
        // borderWidth:0.2,
        // borderRadius:10,
        borderColor:"black",
        margin:10,
        marginLeft:10
    },
    contentView:{
        flexDirection:"column",
        width:"68%",
        marginRight:10,
        paddingBottom:10,
        marginLeft:10
    },
    fab:{
        backgroundColor:"#326bf3",
        borderRadius:100,
        height:50,
        width:50,
        bottom:10,
        right:10,
        // alignSelf:"flex-end",
        // margin:20,
        justifyContent:"center",
        position:"absolute"
    }
})