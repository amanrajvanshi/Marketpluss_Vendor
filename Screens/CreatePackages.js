import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,TextInput,
    ScrollView,Dimensions,TouchableOpacity
} from 'react-native';
import {Icon,Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons"
import MultiSelect from 'react-native-multiple-select';
import RBSheet from 'react-native-raw-bottom-sheet';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from "react-native-image-crop-picker";
import {Picker} from '@react-native-picker/picker';
//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

const options = {
    title: "Pick an Image",
    storageOptions:{
        skipBackup: true,
        path:'images'
    }
}

class CreatePackages extends Component{

    constructor(props){
        super(props);
    }

    //for header left component
    renderLeftComponent(){
        return(
            <View style={{width:win.width,flexDirection:"row",paddingBottom:10,borderBottomWidth:1,borderColor:"#d3d3d3"}} >
                <Icon name="arrow-back-outline"  type="ionicon"
                onPress={()=>this.props.navigation.goBack()} style={{top:2.5}}/>
                <Text style={[styles.h3,{paddingLeft:15,bottom:1}]}>Create Packages</Text> 
            </View>
            )
    }

    render(){
        return(
            <View style={[styles.container]}>
                <View>
                    <Header 
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                            colors: ['#fff', '#fff'],
                        
                        
                        }}
                    />
                </View>

                <ScrollView>
                    <Fields navigation={this.props.navigation}/>
                </ScrollView>

            </View>

        )
    }
}

export default CreatePackages;


const category = [{
    id: '1',
    name: 'Phone'
  }, {
    id: '2',
    name: 'Laptop'
  }, {
    id: '3',
    name: 'Gadgets'
  }, {
    id: '4',
    name: 'Spa'
  }, {
    id: '5',
    name: 'Fashion'
  }, {
    id: '6',
    name: 'Salon'
  }, {
    id: '7',
    name: 'Health'
  }, {
    id: '8',
    name: 'Restaurant'
  }, {
    id: '9',
    name: 'Gym'
    }
];





const items = [{
    id: '1',
    name: 'Oppo'
  }, {
    id: '2',
    name: 'Vivo'
  }, {
    id: '3',
    name: 'Micromax'
  }, {
    id: '4',
    name: 'Redmi'
  }, {
    id: '5',
    name: 'Realme'
  }, {
    id: '6',
    name: 'Samsung'
  }, {
    id: '7',
    name: 'Gionee'
  }, {
    id: '8',
    name: 'OnePluss'
  }, {
    id: '9',
    name: 'Nokia'
    }
];

class Fields extends Component{
    constructor(props) {
        super(props);

        this.state = {
            selectedItems: [],
            selectedCategories:[]
        };
    }

    onSelectedCategoryChange = selectedCategories => {
        this.setState({ selectedCategories});
      };

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
      };

    

    camera =()=>{

        launchCamera(options, (response)=>{
            
            if(response.didCancel){
                console.warn(response)
                console.warn("User cancelled image picker");
            } else if (response.error){
                console.warn('ImagePicker Error: ', response.error);
            }else{
                // const source = {uri: response.assets.uri};
              let path = response.assets.map((path)=>{
                  return (
                      console.warn(path.uri)   
                  )
              })
              this.setState({image:path.uri})
  
                
                
            }
        })
      }
      gallery =()=>{
        ImagePicker.openPicker({
            width:300,
            height:400,
            cropping:true,
        }).then(image=>{
            console.log(image);
            this.setState({image:"Image Uploaded"})

        })
    }
    render(){
        const { selectedItems,selectedCategories } = this.state;
        return(
            <View style={{flex:1,marginBottom:15,}}>
                <View>
                    <Text style={style.fieldsTitle}>
                        Name
                    </Text>
                    <TextInput 
                    style={style.textInput}/>
                </View>
            {/* Category View */}

                <Text style={style.fieldsTitle}>Category</Text>
                        <View style={{marginLeft:20,marginRight:20}}>
                        <MultiSelect
                        hideTags
                        items={category}
                        uniqueKey="id"
                        ref={(cat) => { this.multiSelect = cat }}
                        onSelectedItemsChange={this.onSelectedCategoryChange}
                        
                        selectedItems={selectedCategories}
                        selectText="Select"
                        searchInputPlaceholderText="Search Items..."
                        onChangeInput={ (text)=> console.log(text)}
                        altFontFamily="ProximaNova-Light"
                        tagRemoveIconColor="#CCC"
                        tagBorderColor="#CCC"
                        tagTextColor="#CCC"
                        selectedItemTextColor="green"
                        selectedItemIconColor="green"
                        itemTextColor="#000"
                        displayKey="name"
                        searchInputStyle={{ color: '#CCC' }}
                        submitButtonColor="#326bf3"
                        // submitButtonStyle={{width:50}}
                        submitButtonText="Submit"
                        />
                                    </View>
                                    {/* <Text style={{color:"#fff",marginTop:15, fontSize: 15,paddingLeft:10}} > {this.state.selectedValue}</Text>  */}
                               
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate("AddCategory")} style={{width:70,alignItems:"center",borderRadius:5,alignSelf:"flex-end", height:30,borderWidth:1,marginTop:15,marginRight:20}}>
                            <Text style={styles.h4}>Add</Text>
                        </TouchableOpacity>
                        

                <Text style={style.fieldsTitle}>Product</Text>
                <View style={{marginLeft:20,marginRight:20}}>
                <MultiSelect
          hideTags
          items={items}
          uniqueKey="id"
          ref={(component) => { this.multiSelect = component }}
          onSelectedItemsChange={this.onSelectedItemsChange}
          selectedItems={selectedItems}
          selectText="Select"
          searchInputPlaceholderText="Search Items..."
          onChangeInput={ (text)=> console.log(text)}
          altFontFamily="ProximaNova-Light"
          tagRemoveIconColor="#CCC"
          tagBorderColor="#CCC"
    
          tagTextColor="#CCC"
          selectedItemTextColor="green"
          selectedItemIconColor="green"
          itemTextColor="#000"
          displayKey="name"
          searchInputStyle={{ color: '#CCC' }}
          submitButtonColor="#326bf3"
          submitButtonText="Submit"
        />
        </View>

                <View style={{marginTop:10}}>
                    <Text style={style.fieldsTitle}>
                        Market Price
                    </Text>
                    
                    <TextInput 
                    keyboardType="numeric"
                    style={[style.textInput,{paddingLeft:30}]}/>
                    <Text style={{left:25, top:55,position:"absolute"}} >
                    <MaterialCommunityIcons name="currency-inr" size={20} />
                    </Text>

                </View>

                <View>
                    <Text style={style.fieldsTitle}>
                        Our Price
                    </Text>
                    <TextInput 
                    keyboardType="numeric"
                    style={[style.textInput,{paddingLeft:30}]}/>
                    <Text style={{left:25, top:55,position:"absolute"}} >
                    <MaterialCommunityIcons name="currency-inr" size={20} />
                    </Text>
                </View>

                <View>
                    <Text style={style.fieldsTitle}>
                        Description <Text style={{color:"grey"}}>(50words) </Text>
                    </Text>
                    <TextInput 
                    maxLength={50}
                    style={[style.textInput,{height:80}]}
                    multiline={true}
                    maxLength={100}/>
                </View>

                <View>
                    <View style={{flexDirection:"row",width:"100%"}}>


                        <View style={{width:"60%"}}>
                            <Text style={style.fieldsTitle}>
                            Upload Package Image
                            </Text>
                            <View style={{flexDirection:"column"}}>
                                <Image source={require('../img/abcd.png')}
                                style={style.serviceImg}/>
                                <TouchableOpacity style={style.uploadButton} onPress={()=>this.RBSheet.open()} >
                                    <Text style={style.buttonText}>
                                        Choose Photo
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                {/* Bottom Sheet fot FAB */}
              <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={150}
                        customStyles={{
                            container:{
                                borderTopLeftRadius:20,
                                borderTopRightRadius:20
                            },
                        wrapper: {
                            // backgroundColor: "transparent",
                            borderWidth: 1
                        },
                        draggableIcon: {
                            backgroundColor: "grey"
                        }
                        }}
                    >
                        {/* bottom sheet elements */}
                    <View>
                        
                        {/* Bottom sheet View */}
                            <View style={{width:"100%",padding:20}}>
                            <TouchableOpacity onPress={this.camera}>
                                        <Text style={style.iconPencil}>
                                            <Icon name='camera' type="ionicon" color={'#0077c0'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Take a picture</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={this.gallery} > 
                                        <Text style={style.iconPencil}>
                                            <Icon name='folder' type="ionicon" color={'#0077c0'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Select from library</Text>
                                        </TouchableOpacity>

                            </View>
                        
                    </View>
                    </RBSheet>



                <TouchableOpacity 
                onPress={()=>this.props.navigation.navigate("Service")}
                style={style.buttonStyles}>
                <LinearGradient 
                    colors={['#326BF3', '#0b2564']}
                    style={styles.signIn}>

                    <Text style={[styles.textSignIn, {color:'#fff'}]}>
                    Save</Text>
                </LinearGradient>
                </TouchableOpacity>

            </View>
        )
    }
}

const style=StyleSheet.create({
    fieldsTitle:{
        fontFamily:"Raleway-Regular",
            fontSize:17,
            paddingLeft:20,
            padding:10,
    },
    textInput:{
      borderWidth: 1,
      borderColor:"#d3d3d3",

    //   backgroundColor: '#f5f5f5',
      borderRadius:5,
      padding:5,
      width:Dimensions.get("window").width/1.1,
      height: 40,
      alignContent: 'center',
      alignSelf: 'center',
      fontSize:15,
    },
    buttonStyles:{
        width:"35%",
        alignSelf:"center",
        marginTop:50,
        marginRight:5
    },
    iconPencil:{
        marginLeft:20,
        fontSize:20,
        marginBottom:10
    },
    Text:{
        position:"absolute",
        fontSize:20,
        marginLeft:80,
        fontFamily:"Raleway-Medium"
    },
    serviceImg:{
        height:80,
        width:120,
        borderWidth:1,
        borderColor:"#d3d3d3",
        
        // borderColor:"#000",
        // alignSelf:"center",
        marginLeft:20,
    },
    uploadButton:{
        backgroundColor:"#326bf3",
        width:105,
        height:40,
        justifyContent:"center",
        padding:5,
        borderRadius:5,
        // alignSelf:"center",
        marginLeft:30,
        marginTop:20
    },
    buttonText:{
        fontFamily:"Raleway-SemiBold",
        color:"#fff"
    }
})