import React, { Component } from 'react';
import {
    Text,View,ScrollView,Dimensions,
    StyleSheet,Image,Pressable,
    TouchableOpacity,ImageBackground
} from 'react-native';
import {Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
//Global StyleSheet Import
const styles = require('../Components/Style.js');
const screenWidth = Dimensions.get("window").width;

class Demo extends Component{ 
    render(){
        const chartConfig = {
            backgroundColor:"grey",
            color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            strokeWidth: 3, // optional, default 3
            barPercentage: 3,
            useShadowColorFromDataset: false, // optional
            backgroundGradientFrom: "#1E2923",
            backgroundGradientFromOpacity: 0,
            backgroundGradientTo: "#08130D",
            backgroundGradientToOpacity: 0.5,
          };
          
       

        const data = [
            {
            name: "5 Star",
              population: 9150000,
              color: "blue",
              legendFontColor: "#7F7F7F",
              legendFontSize: 15
            },
            {
              name: "4 Star",
              population: 11920000,
              color: "#228b22",
              legendFontColor: "#7F7F7F",
              legendFontSize: 15
            },
            {
              name: "3 Star",
              population:  8538000,
              color: "#c90016",
              legendFontColor: "#7F7F7F",
              legendFontSize: 15
            },
            {
              name: "2 Star",
              population:7800000,
              color: "orange",
              legendFontColor: "#7F7F7F",
              legendFontSize: 15
            },
            {
              name: "1 Star",
              population: 3527612 ,
              color: "#3a5898",
              legendFontColor: "#7F7F7F",
              legendFontSize: 15
            }
           ];

        return(
            <View style={style.containerMain}>
                
              {/* heading */}
              <Text style={[styles.heading,{color:"#000",paddingTop:10}]}>Your Dashboard</Text>

{/* view for data components */}
<View>
    <View style={{flexDirection:"row",marginTop:10,}}>
        {/* Shop Visits View  */}
        <LinearGradient
         start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
         colors={['#326BF3', '#0b2564']}
         style={style.gradientView}>
             <View style={{flexDirection:"row",marginLeft:-20,marginTop:5}}>
                 <Icon type="ionicon" name="location-outline" color="#fff"
                 style={{marginRight:10,top:2}} size={20}/>
                <Text style={{color:'#fff',fontFamily:"Roboto-Regular",marginTop:4}}>
                Shop Visits 
                </Text>
            </View>
           <Text style={ {color:'#fff',fontFamily:"Roboto-Bold",fontSize:18,marginBottom:10}}>
            56
           </Text>
        </LinearGradient>

        {/* Total Feed Views */}
        <LinearGradient
         start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
         colors={['#326BF3', '#0b2564']}
         style={style.gradientView}>
             <View style={{flexDirection:"row",marginLeft:-20,marginTop:5}}>
                 <Icon type="ionicon" name="eye-outline" color="#fff"
                 style={{marginRight:10,top:2}} size={20}/>
                <Text style={{color:'#fff',fontFamily:"Roboto-Regular",marginTop:4}}>
                Total Feed Views  
                </Text>
            </View>
           <Text style={ {color:'#fff',fontFamily:"Roboto-Bold",fontSize:18,marginBottom:10}}>
            10,012
           </Text>
        </LinearGradient>

    </View>

    {/* Followings */}
    <View style={{flexDirection:"row",marginTop:20,}}>
        <LinearGradient
         start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
         colors={['#326BF3', '#0b2564']}
         style={style.gradientView}>
            <View style={{flexDirection:"row",marginLeft:-20,marginTop:5}}>
                 <Icon type="ionicon" name="person-add-outline" color="#fff"
                 style={{marginRight:10,top:2}} size={20}/>
                <Text style={{color:'#fff',fontFamily:"Roboto-Regular",marginTop:4}}>
                Followings  
                </Text>
            </View>
           <Text style={ {color:'#fff',fontFamily:"Roboto-Bold",fontSize:18,marginBottom:10}}>
            1,225
           </Text>
        </LinearGradient>

        {/* contact us */}
        <LinearGradient
         start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
         colors={['#326BF3', '#0b2564']}
         style={style.gradientView}>
             <View style={{flexDirection:"row",marginLeft:-20,marginTop:5}}>
                 <Icon type="ionicon" name="call-outline" color="#fff"
                 style={{marginRight:10,top:2}} size={20}/>
                <Text style={{color:'#fff',fontFamily:"Roboto-Regular",marginTop:4}}>
                Contact Us 
                </Text>
            </View>
           <Text style={ {color:'#fff',fontFamily:"Roboto-Bold",fontSize:18,marginBottom:10}}>
            122
           </Text>
        </LinearGradient>

    </View>

    {/* total saved feeds */}
    <View style={{marginTop:20}}>
    <LinearGradient
         start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
         colors={['#326BF3', '#0b2564']}
         style={[style.gradientView,{width:"93.5%"}]}>
            <View style={{flexDirection:"row",marginLeft:-20,marginTop:5}}>
                 <Icon type="ionicon" name="bookmarks-outline" color="#fff"
                 style={{marginRight:10,top:2}} size={20}/>
                <Text style={{color:'#fff',fontFamily:"Roboto-Regular",marginTop:4}}>
                Total Saved Feeds  
                </Text>
            </View>
           <Text style={ {color:'#fff',fontFamily:"Roboto-Bold",fontSize:18,marginBottom:10}}>
            121
           </Text>
        </LinearGradient>
    </View>
    
</View>

                {/* Graphs */}

                <View style={{marginTop:15,}}>
                <View style={{backgroundColor:"#d3d3d3",width:"100%",height:1}}></View>
                <View style={{marginTop:10,alignItems:"center",marginBottom:10, marginLeft:-40}} width={Dimensions.get('window').width}>
                    <Text style={[styles.h3,{color:"#326bf3",fontSize:17,alignSelf:"center", marginLeft:90,fontFamily:"Roboto-Bold",marginBottom:20,marginTop:5}]}>
                        Power Zone Analysis
                        </Text>
                    <BarChart
                        data={{
                            labels:["10","20","30","40","50","60","70"],
                            datasets: [
                            {
                                data: [20, 25, 35, 30, 25, 20,24],
                                colors:[
                                    (opacity = 1) => `#3a5898`,
                                    (opacity = 1) => `#3a5898`,
                                    (opacity = 1) =>`#3a5898`,
                                    (opacity = 1) => `#3a5898`,
                                    (opacity = 1) => `#3a5898`,
                                    (opacity = 1) => `#3a5898`,
                                    (opacity = 1) => `#3a5898`,
                                    // (opacity = 1) => `#ffff1f`,
                                ]
                            },
                            ],
                        }}
                        width={Dimensions.get('window').width}
                        height={190}
                        // showValuesOnTopOfBars={true}
                        withInnerLines={true}
                        segments={1}
                        chartConfig={{
                            backgroundGradientFrom: 'white',
                            backgroundGradientTo: 'white',
                            backgroundColor: 'white',
                            backgroundGradientFromOpacity:0,
                            backgroundGradientToOpacity:0,
                            // labelColor: (opacity = 1) => `rgba(0, 0, 0, 1)`,
                            color: (opacity = 1) => "#326bf3",
                            propsForBackgroundLines: {
                                strokeWidth: 1,
                                stroke: 'white',
                                strokeDasharray: '5',
                            },

                        }}
                        flatColor={true}
                        withCustomBarColorFromData={true}
                        showValuesOnTopOfBars={true}
                        fromZero={true}
                        showBarTops={false}
                        withHorizontalLabels={false}
                        />
                    </View>
                    <View style={{backgroundColor:"#d3d3d3",width:"100%",height:1}}></View>
                    <View style= {{width:Dimensions.get('window').width,alignItems:"center", paddingLeft:10}}>
                    
                    {/* Pie chart for ratings */}
                    <PieChart
                    data={data}
                    width={screenWidth}
                    height={230}
                    chartConfig={chartConfig}
                    accessor={"population"}
                    backgroundColor={"transparent"}
                    paddingLeft={"5"}
                    paddingBottom={"15"}
                    paddingTop={"15"}
                    // center={[10, 50]}
                    absolute
                    />
                    </View>
                    <View style={{backgroundColor:"#d3d3d3",width:"100%",height:1}}></View>
                    </View>

            </View>
        )
    }
}

export default Demo;

const style=StyleSheet.create({
    containerMain:{
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        // position:"absolute"
    },
    gradientView:{
        width: '45%',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        flexDirection:"row",
        // justifyContent:"space-evenly",
        marginLeft:15,
        flexDirection:"column",
        justifyContent:"space-around"
    }
})