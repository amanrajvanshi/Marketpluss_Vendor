import React, { Component } from 'react';
import {
    View,ImageBackground,
    StyleSheet,Pressable,ActivityIndicator,
    Image,Text,Dimensions, ScrollView
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Header,Icon} from "react-native-elements";
import { TextInput } from 'react-native-gesture-handler';

//Global Style Import
const styles = require('../Components/Style.js');

class Comments extends Component{
    constructor(props){
        super(props);
    
        this.taskInput=React.createRef();
        this.textInput=React.createRef();
        this.state={
            input:"",
            object:{}
        };
    }

    update() {
  
        if (this.state.input.trim() != '') {
          const object = this.state.object;
          object["Field" + this.counter++] = this.state.input;
          this.setState({ object });
          this.state.input==""
          
        }
      }
      handleFocus =()=>{
        this.taskInput.current.focus()
        
      }
    
    //for header left component
renderLeftComponent(){
    return(
      <View style={{top:10}}>
        <Icon type="ionicon" name="arrow-back-outline"
        onPress={()=>{this.props.navigation.goBack()}}/> 
      </View>
    )
  }
  //for header center component
  renderCenterComponent()
  {
  return(
  <View>
  <Text style={style.text}>Comments</Text>
  </View>
  
  )
  }
    render(){
        return (
        <View style={[styles.container,{backgroundColor:"#fff",height:"100%",borderTopWidth:1,borderColor:"#d3d3d3"}]}>
                               
                <Header 
                statusBarProps={{ barStyle: 'light-content' }}
                centerComponent={this.renderCenterComponent()}
                leftComponent={this.renderLeftComponent()}
                ViewComponent={LinearGradient} // Don't forget this!
                linearGradientProps={{
                colors: ['white', 'white'],
                start: { x: 0, y: 0.5 },
                end: { x: 1, y: 0.5 },
                
                }}
                />
                
                <ScrollView >
                <View style={style.descriptionView}>

                <Image source={require("../img/logo/mp.png")} style={style.profileImage}/>
                <View style={{width:"80%"}}>
                <Text style={style.name}>Mast & Harbour  </Text>
                    <Text style={{marginLeft:10, fontSize:14,fontFamily:"Roboto-Regular", color:"grey"}}>
                        <Text style={{color:"grey"}}>This is the post description  </Text>
                            {"\n"} #description #description #description #description #description #description #description
                    {/* {this.props.route.params.paramKey} */}
                    </Text>
                    <Text style={style.postTime}>3 hours ago</Text>
                    </View>
                </View>

                    {/* Indivudual comment View */}

                <View style={{flexDirection:"row",paddingTop:20,paddingLeft:10,paddingRight:20,paddingBottom:10}}>
                <Image source={require("../img/12.png")} style={style.profileImage}/>
                <View style={{width:"80%"}}>
                <Text style={style.name}>User </Text>
                    <Text style={{marginLeft:10, fontSize:14,         fontFamily:"Roboto-Regular",}}>
                        Location of shop?
                    </Text>
                    <Text style={style.postTime}>1h</Text>
                    </View>
                </View>

                
                    {/* Indivudual comment View */}

                    <View style={{flexDirection:"row",paddingTop:20,paddingLeft:10,paddingRight:20,paddingBottom:10}}>
                <Image source={require("../img/12.png")} style={style.profileImage}/>
                <View style={{width:"80%"}}>
                <Text style={style.name}>User </Text>
                    <Text style={{marginLeft:10, fontSize:14,         fontFamily:"Roboto-Regular",}}>
                        Wanna buy this one
                    </Text>
                    <Text style={style.postTime}>1h</Text>
                    </View>
                </View>

                
                
                {Object.values(this.state.object).map(v => (
                    <Text>
                        <View style={{flexDirection:"row",paddingTop:20,paddingLeft:10,paddingRight:20,paddingBottom:10}}>
                        <Image source={require("../img/12.png")} style={style.profileImage}/>
                <View style={{width:"80%"}}>
                <Text style={style.name}>User </Text>
                    <Text style={{marginLeft:10, fontSize:14,         fontFamily:"Roboto-Regular",}}>
                        {v}
                    </Text>
                    <Text style={style.postTime}>just now</Text>
                    </View>
                    </View>
                    </Text>
                )
                )}
                    

                    </ScrollView>
                    {/* Comment Post View */}
                
                    <View style={{flexDirection:"row",position:"absolute",bottom:0,width:"100%",borderTopWidth:1,borderColor:"#fafafa", paddingLeft:10,backgroundColor:"#f5f5f5", paddingBottom:10}}>
                <Image source={require("../img/12.png")} style={[style.profileImage,{marginTop:10}]}/>
                <TextInput style={{width:"80%",paddingLeft:10,fontSize:14,fontFamily:"Roboto-Regular"}}
                ref={this.taskInput}
                 placeholder="Comment as User"
                 value={this.state.input }
                onChangeText={(v)=>{this.setState({input:v})}} />
                <Text style={{marginTop:15}}>
                <Icon name="send" size={24}  onPress={() => {this.update()}}/>
                </Text>
                </View>
                
              
            </View>
        )
    }
}
export default Comments




//internal stylesheet 
const style=StyleSheet.create({
    text:{
        fontFamily:"Raleway-SemiBold",
        fontSize:20,
        margin:5
    },
    descriptionView:{flexDirection:"row",paddingTop:20,paddingLeft:10,paddingRight:20,paddingBottom:10, borderBottomWidth:0.2},
    
    profileImage:{
        height:37,
        width:37,
        borderRadius:20,
        // marginTop:20,
        // marginLeft:10
    },
    name:{
      fontFamily:"Raleway-Bold",
      fontSize:15,
      marginLeft:10,
      marginTop:-15,
    },
    postTime:{
        fontFamily:"Roboto-Regular",
        color:"grey",
        marginLeft:10
      },

})