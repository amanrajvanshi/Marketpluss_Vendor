import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,TouchableOpacity,
    ScrollView,Dimensions,Linking
} from 'react-native';
import {Icon,Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';


//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class ContactUs extends Component{

    constructor(props){
        super(props);
    }


 //for header left component
 renderLeftComponent(){
    return(
    <View style={{width:win.width,flexDirection:"row"}} >
        <Icon name="arrow-back-outline"  type="ionicon"
        onPress={()=>this.props.navigation.goBack()} style={{top:2.5}}/>
        <Text style={[styles.h3,{paddingLeft:15,bottom:1}]}>Contact Us</Text> 
    </View>
    )
}

    render(){
        return(
            <View>
               <View>
                <Header 
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                        colors: ['#fff', '#fff'],
                    
                    
                    }}
                />
            </View>

            <ScrollView>

            <SupportView />


            {/* Mail View */}

            <Mail />

            {/* FAQ View */}
            <FAQ navigation={this.props.navigation} />

            </ScrollView>

            </View>
        )
    }
}

export default ContactUs;


// Support Top View

class SupportView extends Component{
    render(){
        return(
            <View style={{backgroundColor:"#fff",flexDirection:"row", padding:10}}>
                    <View style={style.leftView}>
                <Text style={[style.heading,{color:"#ffbf0b"}]}>Welcome to</Text>
                <Text style={[style.heading,{color:"#1F449B"}]}>
                 Customer Support</Text>
                 <Text style={styles.p}>
                Please get in touch and we will be happy to help you.
                 </Text>
                 </View>
                 <View style={{width:Dimensions.get("window").width/2}}>
                 <Image source={require("../img/Capture1.png")} 
                 style={{width:100,height:140,alignSelf:"center"}} />
                 </View>
                </View>
        )
    }
}

// FAQ VIew

class FAQ extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            
            <View style={{marginTop:10,backgroundColor:"#fff"}}>
            <Text style={[styles.heading,{fontSize:20,}]}>FAQ</Text>

            {/* Question View */}
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("Answers")}>
            <View style={style.questView} >
                
                <Text style={styles.p}>How can I track my Order?</Text>
                <Icon type="ionicon" name="chevron-forward-outline" />
                
            </View>
            </TouchableOpacity >
                {/* Question View */}
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("Answers")}>    
            <View style={style.questView}>
            <Text style={styles.p}>What is MarketPluss?</Text>
            <Icon type="ionicon" name="chevron-forward-outline" />
        </View>
        </TouchableOpacity>
            {/* Question View */}
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("Answers")}>
            <View style={style.questView}>
            <Text style={styles.p}>How can I find best offers?</Text>
            <Icon type="ionicon" name="chevron-forward-outline" />
            </View>
            </TouchableOpacity>
            {/* Question View */}
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("Answers")}>
            <View style={style.questView}>
            <Text style={styles.p}>How can I place my order?</Text>
            <Icon type="ionicon" name="chevron-forward-outline" />
            </View>
            </TouchableOpacity>
            {/* Question View */}
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("Answers")}>
            <View style={style.questView}>
            <Text style={styles.p}>Can i cancel my order?</Text>
            <Icon type="ionicon" name="chevron-forward-outline" />
            </View>
            </TouchableOpacity>
            {/* Question View */}
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("Answers")}>
            <View style={style.questView}>
            <Text style={styles.p}>Do you deliver Countrywide?</Text>
            <Icon type="ionicon" name="chevron-forward-outline" />
            </View>
            </TouchableOpacity>
            </View>
        )
    }
}

// Mail View
class Mail extends Component{
    render(){
        return(
            <View style={{marginTop:10,backgroundColor:"#fff",padding:10}}>
                    <Text style={[styles.heading,{fontSize:20,marginTop:0, marginLeft:0}]}>
                        Mail us
                    </Text>
                    <Text style={styles.h5} onPress={() => Linking.openURL('mailto:support@marketpluss.com') }>
                        support@marketpluss.com
                    </Text>
                </View>
        )
    }
}

const style=StyleSheet.create({
    heading:{
        color:"#1F449B",
        fontSize:20,
        fontFamily:"Raleway-SemiBold",
        // marginLeft:10
        // marginTop:45,
        // alignSelf:"center"
    },
    leftView:{
        flexDirection:"column",
        marginLeft:10,
        alignSelf:"center",
        width:Dimensions.get("window").width/2},

    questView:{
        padding:10,
        borderBottomWidth:1,
        borderColor:"#d3d3d3",
        flexDirection:"row",
        justifyContent:"space-between"
    }
    
})
