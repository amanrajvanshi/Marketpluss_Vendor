import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,
    TouchableOpacity,Dimensions
} from 'react-native';
import {Icon,Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';


//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class More extends Component{

    constructor(props){
        super(props);
    }


      //for header left component
      renderLeftComponent(){
        return(
        <View style={{width:win.width}} >
            <Text style={[styles.h3]}>More</Text> 
        </View>
        )
    }

    render(){
        return(
            <View style={styles.container}>

            <View>
            <Header 
                statusBarProps={{ barStyle: 'light-content' }}
                leftComponent={this.renderLeftComponent()}
                ViewComponent={LinearGradient} // Don't forget this!
                linearGradientProps={{
                    colors: ['#fff', '#fff'],
                
                
                }}
            />
            </View>

            <Buttons navigation={this.props.navigation}/>
            
            </View>
        )
    }
}

export default More;

//Touchable button

class Buttons extends Component{
    render(){
        return(
            <View style={{borderTopWidth:0.2,borderColor:"#d3d3d3"}}>
                    {/* Profile */}
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("Profile")}>
                        <View style={style.questView} >
                            <View style={{flexDirection:"row"}}>
                                <Image source={require('../img/icons/profile.png')} style={style.Icon}/>
                                <Text style={style.texxt}>Profile</Text>
                            </View>
                            <Image source={require('../img/icons/right-arrow.png')}
                            style={{height:20,width:20,alignSelf:"center"}}/>
                        </View>
                    </TouchableOpacity >
                        
                    {/* About us */}
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("AboutUs")}>    
                        <View style={style.questView}>
                            <View style={{flexDirection:"row"}}>
                                <Image source={require('../img/icons/about.png')} style={style.Icon}/>
                                <Text style={style.texxt}>About Us</Text>
                            </View>
                            <Image source={require('../img/icons/right-arrow.png')}
                            style={{height:20,width:20,alignSelf:"center"}}/>                        
                        </View>
                    </TouchableOpacity>
                    
                    {/* privacy policy */}
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("PrivacyPolicy")}>
                        <View style={style.questView}>
                            <View style={{flexDirection:"row"}}>
                                <Image source={require('../img/icons/pp.png')} style={style.Icon}/>
                                <Text style={style.texxt}>Privacy Policy</Text>
                            </View>
                            <Image source={require('../img/icons/right-arrow.png')}
                            style={{height:20,width:20,alignSelf:"center"}}/>                        
                        </View>
                    </TouchableOpacity>
                   
                   {/* Contact us */}
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ContactUs")}>
                        <View style={style.questView}>
                            <View style={{flexDirection:"row"}}>
                                <Image source={require('../img/icons/phone.png')} style={style.Icon}/>
                                <Text style={style.texxt}>Contact Us</Text>
                            </View>
                            <Image source={require('../img/icons/right-arrow.png')}
                            style={{height:20,width:20,alignSelf:"center"}}/>                        
                        </View>
                    </TouchableOpacity>
                    
                    {/* logout */}
                    <TouchableOpacity>
                        <View style={style.questView}>
                            <View style={{flexDirection:"row"}}>
                                <Image source={require('../img/icons/logout.png')} style={style.Icon}/>
                                <Text style={style.texxt}>Logout</Text>
                            </View>
                            <Image source={require('../img/icons/right-arrow.png')}
                            style={{height:20,width:20,alignSelf:"center"}}/>                        
                        </View>
                    </TouchableOpacity>

                </View>
        )
    }
}

const style=StyleSheet.create({
    icon:{
        margin:10
    },
    image:{
        height:100,
        width:100,
        alignSelf:"center",
        marginTop:50
    },
    questView:{
        padding:10,
        borderBottomWidth:1,
        borderColor:"#d3d3d3",
        flexDirection:"row",
        justifyContent:"space-between"
    },
    texxt:{
        fontSize:18,
        fontFamily:"Roboto-Regular",
        padding:5,
        paddingLeft:30
    },
    Icon:{
        height:30,
        width:30,
        alignSelf:"center",
        marginLeft:10
    }
})