import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,Image,
  useColorScheme,
  View
} from 'react-native';

import {Icon} from "react-native-elements";
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createStackNavigator} from '@react-navigation/stack';

//Import screens 

import MobileLogin from './Screens/MobileLogin';
import OtpVerify from './Screens/OtpVerify';
import CreateShopProfile from './Screens/CreateShopProfile';
import EnableLocation from './Screens/EnableLocation';
import LocationAccess from './Screens/LocationAccess';
import LocationDetails from './Screens/LocationDetails';
import ChooseCategories from './Screens/ChooseCategory';
import AddCover from './Screens/AddCover';
import UnderVerification from './Screens/UnderVerification';
import VerificationDone from './Screens/VerificationDone';
import Home from './Screens/Home';
import TopTab from './Components/TopTab';
import Feeds from './Screens/Feeds';
import More from './Screens/More';
import Offers from './Screens/Offers'
import Shops from './Screens/Services';
import Profile from './Screens/Profile';
import PrivacyPolicy from './Screens/PrivacyPolicy';
import ContactUs from './Screens/ContactUs';
import AboutUs from './Screens/AboutUs';
import Answers from './Screens/Answers';
import CreateOffers from './Screens/CreateOffers';
import Demo from './Screens/Demo';
import CreatePackages from './Screens/CreatePackages';
import CreateService from './Screens/CreateService';
import AddCategory from './Screens/AddCategory';
import Comments from './Screens/Comments';
import Splash from './Screens/Splash';
import MultipleImage from './Screens/MultipleImage';
import Location from './Screens/Location';

//Navigators
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const logout = createStackNavigator();

global.google_key="AIzaSyDG9RC60WCZTRhE2Du-BhOzrEgsCwckN7M";

class StackNav extends Component{
 
  render(){
    return(
      <Stack.Navigator initialRouteName="Splash">
      <Stack.Screen options={{headerShown: false}} name="Splash" component={Splash}/>

        <Stack.Screen options={{headerShown: false}} name="MobileLogin" component={MobileLogin}/>

        <Stack.Screen name="OtpVerify" component={OtpVerify} options={{headerShown: false}}/>

        <Stack.Screen name="CreateShopProfile" component={CreateShopProfile} options={{headerShown: false}}/>

        <Stack.Screen name="EnableLocation" component={EnableLocation} options={{headerShown: false}}/>

        <Stack.Screen name="LocationAccess" component={LocationAccess} options={{headerShown: false}}/>

        <Stack.Screen name="LocationDetails" component={LocationDetails} options={{headerShown: false}}/>

        <Stack.Screen name="ChooseCategories" component={ChooseCategories} options={{headerShown: false}}/>

        <Stack.Screen name="AddCover" component={AddCover} options={{headerShown: false}}/>

        <Stack.Screen name="UnderVerification" component={UnderVerification} options={{headerShown: false}}/>

        <Stack.Screen name="VerificationDone" component={VerificationDone} options={{headerShown: false}}/>
        
        <Stack.Screen name="Home" component={TabNav} options={{headerShown:false}}/>

        <Stack.Screen name="Profile" component={Profile} options={{headerShown:false}}/>

        <Stack.Screen name="AboutUs" component={AboutUs} options={{headerShown:false}}/>

        <Stack.Screen name="ContactUs" component={ContactUs} options={{headerShown:false}}/>

        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} options={{headerShown:false}}/>

        <Stack.Screen name="Answers" component={Answers} options={{headerShown:false}}/>

        <Stack.Screen name="CreateOffers" component={CreateOffers} options={{headerShown:false}}/>

        <Stack.Screen name="CreateService" component={CreateService} options={{headerShown:false}}/>

        <Stack.Screen name="CreatePackages" component={CreatePackages} options={{headerShown:false}}/>
        <Stack.Screen name="AddCategory" component={AddCategory} options={{headerShown:false}}/>
        <Stack.Screen name="Comments" component={Comments} options={{headerShown:false}}/>
        <Stack.Screen name="MultipleImage" component={MultipleImage} options={{headerShown:false}}/>
        <Stack.Screen name="Location" component={Location} options={{headerShown:false}}/>
        {/* <Stack.Screen name="Demo" component={Demo} options={{headerShown:false}}/> */}

      </Stack.Navigator>
    )
  }
}

class TabNav extends Component{
  render(){
    return(
      <Tab.Navigator 
        initialRouteName="Home"
        screenOptions={({route})=>({
        headerShown:false,
        tabBarIcon:({focused, color,tintColor})=>{
          let iconName;
          if(route.name=="Feeds"){
            return(
              <Image source={require('./img/icons/feeds.png')} 
              style={{width:25,height:25,marginTop:5,tintColor: focused ? "": color}}/>
            )
          }
          else if(route.name=="Services"){
            return(
              <Image source={require('./img/icons/services.png')} 
              style={{width:22,height:22,marginTop:5,tintColor: focused ? "": color}}/>
            )
          }
          else if(route.name=="Home"){
            return(
              <Image source={require('./img/icons/mp.png')} 
              style={{width:23,height:23,marginTop:5,tintColor: focused ? "": color}}/>
            )
          }
          else if(route.name=="Offers"){
            return(
              <Image source={require('./img/icons/offers.png')} 
              style={{width:22,height:22,marginTop:5,tintColor: focused ? "": color}}/>
            )
          }
          else if(route.name=="More"){
            return(
              <Image source={require('./img/icons/more.png')} 
              style={{width:22,height:22,marginTop:5,tintColor: focused ? "": color}}/>
            )          }
          return(
            <Icon name={iconName} color={color} type="ionicon" size={22} />
          )
        }
      })}
      tabBarOptions={{
        labelPosition: "below-icon",
        activeTintColor: "#326bf3",
        inactiveTintColor:"#c0c0c0",
        style: {
        backgroundColor: "white",
        height:55,
        borderTopLeftRadius:15,
        borderTopRightRadius:15
      },
      
      labelStyle: {
        fontSize: 14,
        paddingBottom:5,
      },
    }}>

        <Tab.Screen name="Feeds" component={Feeds}/>

        <Tab.Screen name="Services" component={TopTab}/>

        <Tab.Screen name="Home" component={Home}/>

        <Tab.Screen name="Offers" component={Offers}/>

        <Tab.Screen name="More" component={More}/>

      </Tab.Navigator>
    )
  }
}




class App extends Component{
  constructor(props){
    super(props);
    this.state={
        login:true
    }
}
  render(){
    

    return(
      <NavigationContainer>
        <StackNav/>
      </NavigationContainer>
    )
  }
}

export default App;